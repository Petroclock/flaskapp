# coding=utf-8;

import datetime
from enum import Enum

from sqlalchemy import (Column, Integer, String, Text, ARRAY, SmallInteger,
                        DateTime, Boolean, ForeignKey,)
from sqlalchemy.orm import validates
from sqlalchemy import types
from companies.models import Company
from promo_code.models import PromoCode
from database import Base, db_session
from users_companies_settings.models import user_setting


class RolesEnum(Enum):
    """Роли пользователей"""

    USER = 'User'
    BUSINESS = 'Business'


class GenderEnum(Enum):
    """Половой признак"""

    NONE = None
    MALE = 'Male'
    FEMALE = 'Female'


class RegisterTypeEnum(Enum):
    """Способ добавления пользователя в БД"""

    SELF = 0
    IMPORTED = 1
    BACK_OFFICE = 2


class User(Base):

    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    first_name = Column(String(255), nullable=True)
    last_name = Column(String(255))
    middle_name = Column(String(255))
    gender = Column(String(6), types.Enum(GenderEnum))
    phone = Column(String(255))
    email = Column(String(255))
    birthday = Column(DateTime)
    user_agreement = Column(DateTime)
    password = Column(Text)
    registered_date = Column(DateTime, default=datetime.datetime.now)
    photo = Column(Boolean, default=False)
    roles = Column(ARRAY(String), default=['User'])
    promo_code_id = Column(Integer, ForeignKey(PromoCode.id))
    registered_by_company = Column(Integer, ForeignKey(Company.id))
    registered_by_type = Column(SmallInteger, types.Enum(RegisterTypeEnum),
                                default=RegisterTypeEnum.SELF.value)
    last_login_date = Column(DateTime)

    def __init__(self, **kwargs):
        self.id = kwargs.get('id')
        self.first_name = kwargs.get('first_name')
        self.second_name = kwargs.get('second_name')
        self.middle_name = kwargs.get('middle_name')
        self.gender = kwargs.get('gender')
        self.phone = kwargs.get('phone')
        self.email = kwargs.get('email')
        self.birthday = kwargs.get('birthday')
        self.user_agreement = kwargs.get('user_agreement')
        self.password = kwargs.get('password')
        self.registered_date = kwargs.get('registered_date')
        self.photo = kwargs.get('photo')
        self.roles = kwargs.get('roles')
        self.promo_code_id = kwargs.get('promo_code_id')
        self.registered_by_company = kwargs.get('registered_by_company')
        self.registered_by_type = kwargs.get('registered_by_type')
        self.last_login_date = kwargs.get('last_login_date')

    @validates('gender')
    def validate_gender(self, key, gender):
        valid_values = [None, GenderEnum.MALE.value, GenderEnum.FEMALE.value]
        assert gender in valid_values
        return gender

    @validates('roles')
    def validate_roles(self, key, roles):
        valid_roles = [
            None,
            RolesEnum.USER.value,
            RolesEnum.BUSINESS.value
        ]
        if isinstance(roles, list):
            for role in roles:
                assert role in valid_roles
        else:
            assert roles in valid_roles
            roles = [roles]
        return roles

    @validates('registered_by_type')
    def validate_registered_by_type(self, key, registered_by_type):
        valid_types = [
            None,
            RegisterTypeEnum.SELF.value,
            RegisterTypeEnum.IMPORTED.value,
            RegisterTypeEnum.BACK_OFFICE.value
        ]
        assert registered_by_type in valid_types
        return registered_by_type


def new_user(req):
    data = user_data(req)
    try:
        if (data['phone'] or data['password']) is None:
            return
        elif User.query.filter_by(phone=data['phone']).one_or_none():
            return
    except KeyError:
        return
    record = User(**data)
    db_session.add(record)
    db_session.commit()
    db_session.refresh(record)
    data['user_id'] = record.id
    user_setting(data)
    return record


def user_data(form):
    data = dict(form)
    for key, val in data.items():
        data[key] = val[0]
    return data


def get_user(log_in, password):
    user = db_session.query(User) \
        .filter(User.password == password) \
        .filter(User.phone == log_in).one_or_none()
    return user

