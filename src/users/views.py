from flask import request
from lib.request import RequestParams
from .blueprint import users_app
from lib.helpers import json_success, json_fail, parse_query_result
from lib.decorators import json_response
from database import engine as connection
from sqlalchemy import text
from users.models import User
from sqlalchemy.orm.exc import NoResultFound
from database import db_session
from flask import session


@users_app.route('/update-profile', methods=['POST'], endpoint='update_profile')
@json_response
def update_profile():
    try:
        data = RequestParams(request)
    except TypeError:
        return json_fail('Empty request: no data')
    try:
        user = User.query.filter(User.id == session['user_id']).one()
        user.first_name = data.data['firstName']
        user.second_name = data.data['secondName']
        user.gender = data.data['gender']
        user.birthday = data.data['birthday']
        db_session.add(user)
        db_session.commit()
        db_session.refresh(user)
    except NoResultFound:
        return json_fail('Not found')
    except KeyError:
        return json_fail('Empty request: no keys')
    except AssertionError:
        return json_fail('No valid value')
    else:
        return json_success([])
