# coding=utf-8;

from flask import Blueprint
from .models import User

users_app = Blueprint('Users', __name__)
from .views import update_profile
