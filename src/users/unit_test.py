from database import db_session
from lib.tests import AvendaTestCase
from users.models import User
import datetime
from lib.helpers import ResponseStatus


class UserTest(AvendaTestCase):
    def setUp(self):
        self.data = {}
        self.link = '/api/v2/users/'
        self.update_profile_link = 'update-profile'
        self.model = User

    def test_db(self):
        self.db(self.model, self.data)

    def update_session(self, value):
        with self.app as c:
            with c.session_transaction() as sess:
                sess['user_id'] = value

    def test_update_profile_success(self):
        db_user = self.create_insert(self.model, self.data)
        data = {'data': {'firstName': 'Test',
                         'secondName': 'Test',
                         'gender': 'Male',
                         'birthday': datetime.datetime.now()}}
        self.update_session(db_user.id)
        response = self.request_success(self.link + self.update_profile_link, data)
        db_session.add(db_user)
        db_session.refresh(db_user)
        self.assertEqual(response['result'], ResponseStatus.SUCCESS.value)
        self.assertEqual(db_user.first_name, data['data']['firstName'])
        self.assertEqual(db_user.second_name, data['data']['secondName'])
        self.assertEqual(db_user.gender, data['data']['gender'])
        self.assertEqual(db_user.birthday, data['data']['birthday'])
        self.delete_row(db_user)

    def test_update_profile_fail(self):
        db_user = self.create_insert(self.model, self.data)
        self.update_session(db_user.id)
        data = {'data': {}}
        self.request_fail(self.link + self.update_profile_link, data)
        data = {'data': {'firstName': 'Test',
                         'secondName': 'Test',
                         'gender': 'Test',
                         'birthday': datetime.datetime.now()}}
        self.request_fail(self.link + self.update_profile_link, data)
        self.update_session(0)
        data = {'data': {'firstName': 'Test',
                         'secondName': 'Test',
                         'gender': 'Male',
                         'birthday': datetime.datetime.now()}}
        self.request_fail(self.link + self.update_profile_link, data)
        db_session.add(db_user)
        db_session.refresh(db_user)
        null_val = None
        self.assertEqual(db_user.first_name, null_val)
        self.assertEqual(db_user.second_name, null_val)
        self.assertEqual(db_user.gender, null_val)
        self.assertEqual(db_user.birthday, null_val)
        self.delete_row(db_user)
