from flask import Blueprint
from bookings.models import Booking


bookings_app = Blueprint('Bookings', __name__)
from .views import booking