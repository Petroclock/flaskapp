from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, DateTime, JSON, DECIMAL, Float
from enum import Enum
from sqlalchemy.orm import validates
from datetime import datetime


class Status(Enum):

    NOT_ACTIVATED = 'Not_Activated'
    ACTIVATED = 'Activated'
    CANCELED = 'Canceled'
    RETURNED = 'Returned'
    COMPLETED = 'Completed'


class Booking(Base):
    __tablename__ = 'bookings'
    id = Column(Integer, primary_key=True)
    company_id = Column(Integer, ForeignKey('companies.id'))
    added_user = Column(Integer, ForeignKey('users.id'))
    user_id = Column(Integer, ForeignKey('users.id'))
    service_id = Column(Integer, ForeignKey('services.id'))
    plant_id = Column(Integer, ForeignKey('plants.id'))
    payment_method = Column(Integer, ForeignKey('payment_methods.id'))
    cart_id = Column(Integer, ForeignKey('carts.id'))
    order_id = Column(Integer, ForeignKey('orders.id'))
    activate_user = Column(Integer, ForeignKey('users.id'))
    canceled_user = Column(Integer, ForeignKey('users.id'))
    parent = Column(Integer, ForeignKey('bookings.id'))
    fiscal_receipt_printed_user = Column(Integer, ForeignKey('users.id'))
    edited_user = Column(Integer, ForeignKey('users.id'))
    quantity = Column(Integer)
    discount = Column(Integer)
    number = Column(Integer)
    price = Column(Integer)
    sum = Column(Integer)
    receipts_used = Column(Integer, default=0)
    receipts_printed = Column(Integer, default=0)
    refund_sum = Column(DECIMAL, default=0.0)
    fee_sum = Column(DECIMAL, default=0.0)
    agent_sum = Column(DECIMAL, default=0.0)
    company_sum = Column(DECIMAL, default=0.0)
    fee_com = Column(Float)
    customer_first_name = Column(String)
    customer_second_name = Column(String)
    service_name = Column(String)
    status = Column(String)
    phone_number = Column(String)
    user_first_name = Column(String)
    user_last_name = Column(String)
    comment = Column(String)
    order_number = Column(String)
    is_ordered = Column(Boolean)
    added_date = Column(DateTime, default=datetime.now())
    target_time = Column(DateTime)
    activate_date = Column(DateTime)
    edited_date = Column(DateTime)
    completed_date = Column(DateTime)
    payed_date = Column(DateTime)
    canceled_date = Column(DateTime)
    fiscal_receipt_printed_date = Column(DateTime)
    properties = Column(JSON, default={})

    def __init__(self, **kwargs):
        self.company_id = kwargs.get('company_id')
        self.is_ordered = kwargs.get('is_ordered')
        self.added_date = kwargs.get('added_date')
        self.added_user = kwargs.get('added_user')
        self.quantity = kwargs.get('quantity')
        self.user_id = kwargs.get('user_id')
        self.customer_first_name = kwargs.get('customer_first_name')
        self.customer_second_name = kwargs.get('customer_second_name')
        self.discount = kwargs.get('discount')
        self.number = kwargs.get('number')
        self.service_id = kwargs.get('service_id')
        self.service_name = kwargs.get('service_name')
        self.phone_number = kwargs.get('phone_number')
        self.plant_id = kwargs.get('plant_id')
        self.price = kwargs.get('price')
        self.status = kwargs.get('status')
        self.sum = kwargs.get('sum')
        self.target_time = kwargs.get('target_time')
        self.payment_method = kwargs.get('payment_method')
        self.cart_id = kwargs.get('cart_id')
        self.order_id = kwargs.get('order_id')
        self.order_number = kwargs.get('order_number')
        self.activate_user = kwargs.get('activate_user')
        self.canceled_user = kwargs.get('canceled_user')
        self.parent = kwargs.get('parent')
        self.fiscal_receipt_printed_user = kwargs.get('fiscal_receipt_printed_user')
        self.edited_user = kwargs.get('edited_user')
        self.receipts_used = kwargs.get('receipts_used')
        self.receipts_printed = kwargs.get('receipts_printed')
        self.user_first_name = kwargs.get('user_first_name')
        self.user_last_name = kwargs.get('user_last_name')
        self.comment = kwargs.get('comment')
        self.refund_sum = kwargs.get('refund_sum')
        self.fee_sum = kwargs.get('fee_sum')
        self.agent_sum = kwargs.get('agent_sum')
        self.company_sum = kwargs.get('company_sum')
        self.fee_com = kwargs.get('fee_com')
        self.activate_date = kwargs.get('activate_date')
        self.edited_date = kwargs.get('edited_date')
        self.completed_date = kwargs.get('completed_date')
        self.payed_date = kwargs.get('payed_date')
        self.canceled_date = kwargs.get('canceled_date')
        self.fiscal_receipt_printed_date = kwargs.get('fiscal_receipt_printed_date')
        self.properties = kwargs.get('properties')

    @validates('status')
    def validates_status(self, key, status):
        if status is None:
            return status
        values = [item.value for item in Status]
        if isinstance(status, list):
            for stats in status:
                assert stats in values
        else:
            assert status in values
            status = [status]
        return status
