from bookings.models import Booking
from database import db_session
from lib.tests import AvendaTestCase


class BookingTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.added_user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.service = self.create_service(self.comp.id)
        self.plant = self.create_plant(self.comp.id)
        self.data = {'user_id': self.user.id, 'company_id': self.comp.id, 'added_user': self.added_user.id,
                     'service_id': self.service.id, 'plant_id': self.plant.id}
        self.link = '/api/v2/bookings/'
        self.log_print_link = 'log-print-receipt'
        self._items_to_delete = [self.plant, self.service, self.comp, self.added_user, self.user]
        self.model = Booking

    def test_db(self):
        self.db(self.model, self.data)

    def test_view_success(self):
        record = self.create_insert(self.model, self.data)
        data = {'filters': {'limit': 10,
                            'conditions': [{
                                "field": "id",
                                "condition": "=",
                                "value": record.id
                            }
                            ]},
                'data': {}}
        response = self.request_success(self.link, data)
        for item in (self.service, self.user, self.comp, self.added_user, self.plant):
            db_session.add(item)
        self.assertEqual(response['total'], 1)
        self.assertEqual(len(response['data']), 1)
        self.assertEqual(response['data'][0]['id'], record.id)
        self.assertEqual(response['data'][0]['service_id'], self.service.id)
        self.assertEqual(response['data'][0]['user_id'], self.user.id)
        self.assertEqual(response['data'][0]['company_id'], self.comp.id)
        self.assertEqual(response['data'][0]['added_user'], self.added_user.id)
        self.assertEqual(response['data'][0]['plant_id'], self.plant.id)
        self.delete_row(record)

    def test_view_fail(self):
        self.empty_request(self.link)

    def test_log_print_success(self):
        record = self.create_insert(self.model, self.data)
        self.assertIsNone(record.quantity)
        data = {'data': {'bookingNumber': record.id,
                         'quantity': 1}}
        self.request_success(self.link + self.log_print_link, data)
        self.assertEqual(record.quantity, 1)
        self.delete_row(record)

    def test_log_print_fail(self):
        record = self.create_insert(self.model, self.data)
        data = {'data': {}}
        self.request_fail(self.link + self.log_print_link, data)
        data = {'data': {'bookingNumber': 0,
                         'quantity': 1}}
        self.request_fail(self.link + self.log_print_link, data)
        data = {'data': {'bookingNumber': record.id,
                         'quantity': 'q'}}
        self.request_fail(self.link + self.log_print_link, data)
        self.delete_row(record)
