from flask import request
from lib.request import RequestParams
from .blueprint import bookings_app
from lib.helpers import json_success, json_fail, parse_query_result
from lib.decorators import json_response
from database import engine as connection
from sqlalchemy import text
from bookings.models import Booking
from sqlalchemy.orm.exc import NoResultFound
from database import db_session


@bookings_app.route('/', methods=['POST'], endpoint='booking')
@json_response
def booking():
    try:
        data = RequestParams(request)
    except TypeError:
        return json_fail('Empty request')
    sql = data.sql_expression('bookings')
    result = connection.execute(text(sql))
    response = parse_query_result(result)
    return json_success(response, len(response))


@bookings_app.route('/log-print-receipt', methods=['POST'], endpoint='log_print_receipt')
@json_response
def log_print_receipt():
    try:
        data = RequestParams(request)
    except TypeError:
        return json_fail('Empty request')
    try:
        booking_id = data.data['bookingNumber']
        quantity = data.data['quantity']
        book = Booking.query.filter(Booking.id == booking_id).one()
        book.quantity = int(quantity)
        db_session.add(book)
        db_session.commit()
        db_session.refresh(book)
    except NoResultFound:
        return json_fail('Not found')
    except ValueError:
        return json_fail('No int value')
    except KeyError:
        return json_fail('Empty request')
    else:
        return json_success([])
