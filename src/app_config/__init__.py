# coding=utf-8;
import datetime

DEBUG = False
SECRET_KEY = 'mysecretkey'
DATABASE_URI = 'sqlite:////tmp/myapp.db'
HOST = '127.0.0.1'
PORT = 5000
TESTING = DEBUG
# TODO Подстаить нужное значение
ONE_TIME_PASSWORD_EXPIRATION_TIME_SPAN = datetime.timedelta(days=5)
CART_LIFETIME = datetime.timedelta(days=5)

try:
    from .local_config import *
except ImportError:
    print('Local config is not defined')
