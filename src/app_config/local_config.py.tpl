# coding=utf-8;

DEBUG = False
SECRET_KEY = 'mysecretkey'
DATABASE_URI = 'postgresql:///app'
HOST = '127.0.0.1'
PORT = 5000
TESTING = DEBUG
