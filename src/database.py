from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

import app_config

engine = create_engine(app_config.DATABASE_URI,
                       convert_unicode=True,
                       echo=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    from companies.models import Company
    from users.models import User
    from login_logs.models import LoginLog
    from one_time_passwords.models import OneTimePassword
    from promo_code.models import PromoCode
    from services.models import Service
    from services_prices.models import ServicesPrice
    from users_groups.models import UsersGroup
    from sessions.models import Session
    from users_companies_settings.models import UsersCompaniesSetting
    Base.metadata.create_all(bind=engine)

