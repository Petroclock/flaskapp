from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, JSON
from enum import Enum
from sqlalchemy.orm import validates
from datetime import datetime

from lib.validators import validate_value_in_enum


class Level(Enum):

    LOG = 'log'
    WARNING = 'warning'
    ERROR = 'error'


class Log(Base):
    __tablename__ = 'logs'
    id = Column(Integer, primary_key=True)
    added_date = Column(DateTime, default=datetime.now())
    added_user = Column(Integer, ForeignKey('users.id'))
    session_id = Column(Integer, ForeignKey('sessions.id'))
    request = Column(JSON)
    response = Column(JSON)
    url = Column(String)
    level = Column(String)
    exception = Column(String)

    def __init__(self, **kwargs):
        self.added_date = kwargs.get('added_date')
        self.added_user = kwargs.get('added_user')
        self.session_id = kwargs.get('session_id')
        self.request = kwargs.get('request')
        self.response = kwargs.get('response')
        self.url = kwargs.get('url')
        self.level = kwargs.get('level')
        self.exception = kwargs.get('exception')

    @validates('level')
    def validates_level(self, key, level):
        return validate_value_in_enum(level, Level)
