from lib.tests import AvendaTestCase
from logs.models import Log


class LogTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.session = self.create_session(self.user.id)
        self.data = {'added_user': self.user.id, 'session_id': self.session.id}
        self.link = '/api/v2/logs/'
        self._items_to_delete = [self.session, self.user]
        self.model = Log

    def test_db(self):
        self.db(self.model, self.data)
