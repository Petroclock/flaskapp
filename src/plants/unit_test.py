import unittest
from .models import Plant
from lib.tests import AvendaTestCase


class PlantTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.data = dict(name='test', company_id=self.comp.id)
        self.link = '/api/v2/plants/'
        self._items_to_delete = [self.comp, self.user]
        self.model = Plant

    def test_db(self):
        self.db(self.model, self.data)

    def test_view_success(self):
        plant = self.create_plant(self.comp.id)
        data = {'filters': {'limit': 10,
                            'conditions': [{
                                "field": "company_id",
                                "condition": "=",
                                "value": self.comp.id
                            }]},
                'data': {}}
        response = self.request_success(self.link, data)
        self.assertEqual(response['total'], 1)
        self.assertEqual(len(response['data']), 1)
        self.assertEqual(response['data'][0]['id'], plant.id)
        self.assertEqual(response['data'][0]['company_id'], self.comp.id)
        self.delete_row(plant)

    def test_view_fail(self):
        self.empty_request(self.link)


if __name__ == '__main__':
    unittest.main()
