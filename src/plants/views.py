from flask import request
from lib.request import RequestParams
from .blueprint import plants_app
from sqlalchemy import text
from database import engine as connection
from lib.helpers import json_success, json_fail, parse_query_result
from lib.decorators import json_response


@plants_app.route('/', methods=['POST'])
@json_response
def plant():
    try:
        data = RequestParams(request)
    except TypeError:
        return json_fail('Empty request')
    sql = data.sql_expression('plants')
    result = connection.execute(text(sql))
    response = parse_query_result(result)
    return json_success(response, len(response))
