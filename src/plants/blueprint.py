from flask import Blueprint
from .models import Plant


plants_app = Blueprint('Plants', __name__)
from .views import plant
