from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey
from companies.models import Company


class Plant(Base):
    __tablename__ = 'plants'
    id = Column(Integer, primary_key=True)
    company_id = Column(Integer, ForeignKey(Company.id))
    name = Column(String(255))
    timeZone = Column(String(255))
    timeZoneOffset = Column(Integer)
    weekSchedule = Column(String(255))

    def __init__(self, **kwargs):
        self.company_id = kwargs.get('company_id')
        self.name = kwargs.get('name')
        self.timeZone = kwargs.get('timeZone')
        self.timeZoneOffset = kwargs.get('timeZoneOffset')
        self.weekSchedule = kwargs.get('weekSchedule')
