from slots_types.models import SlotsType
from lib.tests import AvendaTestCase


class SlotsTypeTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.data = {'company_id': self.comp.id}
        self.link = '/api/v2/slots_types/'
        self._items_to_delete = [self.comp, self.user]
        self.model = SlotsType

    def test_db(self):
        self.db(self.model, self.data)
