from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey, Boolean


class SlotsType(Base):
    __tablename__ = 'slots_types'
    id = Column(Integer, primary_key=True)
    company_id = Column(Integer, ForeignKey('companies.id'))
    name = Column(String)
    description = Column(String)
    is_public = Column(Boolean, default=False)
    letter = Column(String)

    def __init__(self, **kwargs):
        self.company_id = kwargs.get('company_id')
        self.name = kwargs.get('name')
        self.description = kwargs.get('description')
        self.is_public = kwargs.get('is_public')
        self.letter = kwargs.get('letter')
