from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey


class UsersGroup(Base):
    __tablename__ = 'users_groups'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    description = Column(String(255))
    from companies.models import Company
    company_id = Column(Integer, ForeignKey(Company.id))
    total = Column(Integer)

    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.description = kwargs.get('description')
        self.company_id = kwargs.get('company_id')
        self.total = kwargs.get('total')
