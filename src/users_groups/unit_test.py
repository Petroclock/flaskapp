import unittest
from .models import UsersGroup
from lib.tests import AvendaTestCase


class UserGroupTest(AvendaTestCase):

    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.data = {
            'name': 'test',
            'description': 'test',
            'company_id': self.comp.id,
            'total': 100
        }
        self._items_to_delete = [self.comp, self.user]
        self.model = UsersGroup

    def test_db(self):
        self.db(self.model, self.data)


if __name__ == '__main__':
    unittest.main()
