from enum import Enum
import decimal
import json
from flask import make_response, session
import datetime


class ResponseStatus(Enum):
    SUCCESS = "success"
    FAIL = "fail"


def json_success(data, count=None):
    return {
        "result": ResponseStatus.SUCCESS.value,
        "error": "",
        "data": data,
        "total": count
    }


def json_fail(message):
    return {
        "result": ResponseStatus.FAIL.value,
        "error": message,
        "data": None,
        "total": None
    }


def parse_query_result(cursor):
    keys = cursor.keys()
    rows = cursor.fetchall()
    response = []
    for row in rows:
        index = 0
        res = {}
        while index < len(row):
            res[keys[index]] = row[index]
            index += 1
        response.append(res)
    return response


class ResponseJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        if isinstance(o, datetime.datetime):
            return o.isoformat()

        return super(ResponseJSONEncoder, self).default(o)


def make_json(response):
    return make_response(json.dumps(response, cls=ResponseJSONEncoder))


def get_user():
    return session['user_id']
