
class RequestParams:

    default_limit = 10

    def __init__(self, request, **kwargs):
        data = request.get_json()
        self.limit = self.get_limit(data)
        self.filters = self.get_filters(data)
        self.data = self.get_data(data)

    def get_limit(self, request_data):
        if not 'filters' in request_data:
            return self.default_limit
        return request_data['filters'].get('limit', self.default_limit)

    @staticmethod
    def get_filters(request_data):
        if not 'filters' in request_data or not 'conditions' in request_data['filters']:
            return []
        filters = request_data['filters']['conditions']
        list_filter = []
        for item in filters:
            obj = request_filter_factory(item['condition'],
                                         item['field'],
                                         item['value'])
            list_filter.append(obj)
        return list_filter

    @property
    def has_where(self):
        return self.filters

    def to_filter_row(self):
        if not self.has_where:
            return ''
        filter_row = ' AND '.join([item.to_sql() for item in self.filters])
        return 'WHERE {}'.format(filter_row)

    @staticmethod
    def get_data(request_data):
        return request_data['data']

    def sql_expression(self, table_name):
        return 'SELECT * FROM {table_name} {where_part}'.format(table_name=table_name, where_part=self.get_where_part())

    def get_where_part(self):
        return '{filter_row} LIMIT {limit}'.format(filter_row=self.to_filter_row(), limit=self.limit)


def request_filter_factory(condition, field_name, value):
    cls_map = {
        '=': EqualsFilter,
        '>': MoreFilter,
        '!=': NotEqualsFilter,
        '<': LessFilter
    }

    try:
        return cls_map[condition](field_name, value)
    except KeyError:
        raise RequestConditionError


class BaseRequestFilter:

    # condition (значение - одно из =,!=, <, >)
    condition = None

    def __init__(self, field_name, value):
        self.field = field_name
        self.value = value

    def validate(self, field_name, value):
        return True

    def to_sql(self):
        self.field = self.replace_quotes(self.field)
        self.value = self.replace_quotes(self.value)
        return self.field + self.condition + str(self.value)

    @staticmethod
    def replace_quotes(value):
        if isinstance(value, str):
            value = value.replace("\\", '')
            value = value.replace('"', '')
            value = value.replace("'", '')
        return value


class EqualsFilter(BaseRequestFilter):
    condition = '='

    def to_sql(self):
        self.field = self.replace_quotes(self.field)
        if isinstance(self.value, (list, set, tuple)):
            return self.field + ' IN ' + self.parse_value(str(self.value))
        self.value = self.replace_quotes(self.value)
        return self.field + self.condition + str(self.value)

    @staticmethod
    def parse_value(val):
        val = val.replace('[', '(')
        val = val.replace(']', ')')
        return val


class NotEqualsFilter(BaseRequestFilter):
    condition = '!='


class MoreFilter(BaseRequestFilter):
    condition = '>'


class LessFilter(BaseRequestFilter):
    condition = '<'


class RequestConditionError(Exception):
    pass
