from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from database import Base
from flaskr import app
from lib.data import Data

migrate = Migrate(app, Base)
manager = Manager(app)

manager.add_command('db', MigrateCommand)


@manager.command
def fill_db():
    Data().fill_db()


@manager.command
def clean_db():
    Data().clean_db()

if __name__ == '__main__':
    manager.run()
