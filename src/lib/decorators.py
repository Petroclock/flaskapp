from lib.helpers import make_json


def json_response(f):

    def response(*args, **kwargs):
        result = f(*args, **kwargs)
        return make_json(result)

    return response
