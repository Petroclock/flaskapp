# coding=utf-8;


def validate_value_in_enum(value, enum_cls):
    if value is None:
        return value
    values = [item.value for item in enum_cls]
    assert value in values
    return value
