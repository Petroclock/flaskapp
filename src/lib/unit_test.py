import unittest
import json
from lib.tests import AvendaTestCase
from flask import Response
from flaskr import app, db_session
from flask import session
from lib import decorators, request, helpers
from users.models import User
from users_companies_settings.models import UsersCompaniesSetting


class DecoratorTest(unittest.TestCase):
    fail_message = 'Failed'
    total_count = 10

    @decorators.json_response
    def get_success_view(self):
        return helpers.json_success({'test': 'yes'}, self.total_count)

    @decorators.json_response
    def get_fail_view(self):
        return helpers.json_fail(self.fail_message)

    @staticmethod
    def parse_data(response):
        data = response.data
        return json.loads(data.decode('utf8'))

    def test_success_view(self):
        with app.app_context():
            with app.test_request_context('/'):
                request = self.get_success_view()
        self.assertIsInstance(request, Response)
        data = self.parse_data(request)
        self.assertTrue(data['result'] == helpers.ResponseStatus.SUCCESS.value)
        self.assertIsNotNone(data['data'])
        self.assertTrue(len(data['error']) == 0)
        self.assertTrue(data['total'] == self.total_count)

    def test_fail_view(self):
        with app.app_context():
            with app.test_request_context('/'):
                request = self.get_fail_view()
        self.assertIsInstance(request, Response)
        data = self.parse_data(request)
        self.assertTrue(data['result'] == helpers.ResponseStatus.FAIL.value)
        self.assertIsNone(data['data'])
        self.assertEqual(self.fail_message, data['error'])
        self.assertIsNone(data['total'])


class TestRequest(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_request(self):
        req = RequestFlask()
        res = request.RequestParams(req)
        self.assertEqual(res.limit, req.limit)
        self.assertEqual(res.data, req.data)

        for i, res_filter in enumerate(res.filters):
            self.assertIsInstance(res_filter, req.conditions[i])
            self.assertEqual(res_filter.to_sql(), req.sql[i])


class TestLogger(AvendaTestCase):
    def setUp(self):
        self.login_link = '/login'
        self.logout_link = '/logout'
        self.reg_link = '/registration'
        self.model = User

    def login(self, phone, password, user_id, success=True):
        data = dict(login=phone, password=password)
        with self.app:
            # res = self.request_success(self.login_link, data)
            res = self.app.post(self.login_link, data=data)
            print(res, res.data)
            print(session)
            if success:
                self.assertEqual(session['user_id'], user_id)
                self.assertTrue(session['is_login'])
            else:
                self.assertIsNone(session['user_id'])
                self.assertFalse(session['is_login'])

    def logout(self, success=True):
        with self.app:
            self.app.get(self.logout_link)
            if success:
                self.assertIsNone(session['user_id'])
                self.assertFalse(session['is_login'])
            else:
                self.assertIsNotNone(session['user_id'])
                self.assertTrue(session['is_login'])

    def registration(self, phone, password):
        data = dict(phone=phone, password=password)
        return self.app.post(self.reg_link, data=data)

    def test_logger_success(self):
        login = '89997776655'
        password = 'TestPass'
        user = self.model.query.filter_by(phone=login, password=password).one_or_none()
        self.assertIsNone(user)
        self.registration(login, password)
        user = self.model.query.filter_by(phone=login, password=password).one_or_none()
        self.assertIsInstance(user, User)
        setting = UsersCompaniesSetting.query.filter_by(user_id=user.id).one_or_none()
        self.assertIsInstance(setting, UsersCompaniesSetting)
        self.login(login, password, user.id)
        self.logout()
        self.delete_row(setting)
        self.delete_row(user)

    def test_logger_fail(self):
        data = {'phone': 'test', 'password': 'test'}
        user = self.create_insert(self.model, data)
        self.registration(data['phone'], data['password'])
        self.login('', '', user.id, success=False)
        self.login(data['phone'], data['password'], user.id)
        self.logout()
        db_session.query(self.model).delete()
        db_session.commit()


class RequestFlask:
    def __init__(self):
        self.limit = 100
        self.conditions = [request.EqualsFilter, request.NotEqualsFilter,
                           request.LessFilter, request.MoreFilter]
        self.sql = ['test=value', 'test!=value', 'test<value', 'test>value']
        self.data = {'test_key': 'test_value'}

    def get_json(self):
        data = {"filters": {
            "limit": self.limit,
            "conditions": [
                {
                    "field": "'test",
                    "condition": "=",
                    "value": '"value"'
                },
                {
                    "field": "''test",
                    "condition": "!=",
                    "value": 'value""'
                },
                {
                    "field": "\\test\'",
                    "condition": "<",
                    "value": '\"value"'
                },
                {
                    "field": "'\'test",
                    "condition": ">",
                    "value": 'value'
                }
            ]
        },
            "data": self.data}
        return data
