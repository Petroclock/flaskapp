import unittest
import json
from json.decoder import JSONDecodeError
from flaskr import app
from database import db_session
from accounts.models import Account
from bookings.models import Booking
from carts.models import Cart
from companies.models import Company
from orders.models import Order
from payment_methods.models import PaymentMethod
from plants.models import Plant
from services.models import Service
from sessions.models import Session
from users.models import User
from users_companies_settings.models import UsersCompaniesSetting
from users_groups.models import UsersGroup
from lib.helpers import ResponseJSONEncoder


class AvendaTestCase(unittest.TestCase):
    app = app.test_client()

    _items_to_delete = []

    def tearDown(self):
        for item in self._items_to_delete:
            self.delete_row(item)

        super().tearDown()

    def create_insert(self, model, data):
        record = model(**data)
        db_session.add(record)
        db_session.commit()
        db_session.refresh(record)
        return record

    def db(self, model, data):
        record = self.create_insert(model, data)
        row = model.query.filter(model.id == record.id).first()
        self.assertEqual(record, row)
        db_session.query(model).filter_by(id=row.id).delete()
        db_session.commit()
        row = model.query.filter(model.id == record.id).first()
        self.assertIsNone(row)

    def request_success(self, link, data):
        request = self.app.open(path=link, method='POST', content_type='application/json',
                                data=json.dumps(data, cls=ResponseJSONEncoder))
        try:
            return json.loads(request.data.decode('utf8'))
        except JSONDecodeError:
            return request

    def request_fail(self, link, data):
        response = self.request_success(link, data)
        self.assertEqual(response['total'], None)
        self.assertEqual(response['data'], None)

    def empty_request(self, link):
        result = self.app.open(path=link, method='POST')
        response = json.loads(result.data.decode('utf8'))
        self.assertEqual(response['total'], None)
        self.assertEqual(response['data'], None)

    @staticmethod
    def create_service(comp_id):
        service = Service(company_id=comp_id, name='test')
        db_session.add(service)
        db_session.commit()
        db_session.refresh(service)
        return service

    @staticmethod
    def create_user():
        user = User(first_name='user', roles=['User'])
        db_session.add(user)
        db_session.commit()
        db_session.refresh(user)
        return user

    @staticmethod
    def create_plant(company_id):
        plant = Plant(company_id=company_id, name='test')
        db_session.add(plant)
        db_session.commit()
        db_session.refresh(plant)
        return plant

    @staticmethod
    def get_or_create_company(user_id):
        comp = Company.query.filter(Company.default_user_id == user_id).first()
        if not isinstance(comp, Company):
            comp = Company(name='test', default_user_id=user_id)
            db_session.add(comp)
            db_session.commit()
            db_session.refresh(comp)
        return comp

    @staticmethod
    def create_user_setting(company_id, user_id):
        setting = UsersCompaniesSetting(company_id=company_id, user_id=user_id)
        db_session.add(setting)
        db_session.commit()
        db_session.refresh(setting)
        return setting

    @staticmethod
    def create_group(comp):
        group = UsersGroup(name='test', company_id=comp.id)
        db_session.add(group)
        db_session.commit()
        db_session.refresh(group)
        return group

    @staticmethod
    def create_book(company_id, user_id, service_id, plant_id):
        record = Booking(company_id=company_id, user_id=user_id, service_id=service_id, plant_id=plant_id,
                         added_user=user_id)
        db_session.add(record)
        db_session.commit()
        db_session.refresh(record)
        return record

    @staticmethod
    def create_payment_method():
        record = PaymentMethod()
        db_session.add(record)
        db_session.commit()
        db_session.refresh(record)
        return record

    @staticmethod
    def create_session(user_id):
        record = Session(user_id=user_id)
        db_session.add(record)
        db_session.commit()
        db_session.refresh(record)
        return record

    @staticmethod
    def create_cart(user_id, company_id, bookings_id, payment_method_id):
        record = Cart(user_id=user_id, added_user=user_id, edited_user=user_id,
                      company_id=company_id, bookings=[bookings_id], payment_method=payment_method_id)
        db_session.add(record)
        db_session.commit()
        db_session.refresh(record)
        return record

    @staticmethod
    def create_account(user_id, company_id):
        record = Account(company_id=company_id, user_id=user_id)
        db_session.add(record)
        db_session.commit()
        db_session.refresh(record)
        return record

    @staticmethod
    def create_order(user_id, cart_id, payment_method_id):
        record = Order(added_user=user_id, edited_user=user_id, cart_id=cart_id, payment_method=payment_method_id)
        db_session.add(record)
        db_session.commit()
        db_session.refresh(record)
        return record

    @staticmethod
    def delete_row(obj):
        db_session.add(obj)
        db_session.query(obj.__class__).filter_by(id=obj.id).delete()
        db_session.commit()
