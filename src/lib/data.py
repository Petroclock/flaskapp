from database import db_session
from accounts.models import Account
from bookings.models import Booking
from carts.models import Cart
from companies.models import Company
from login_logs.models import LoginLog
from logs.models import Log
from messages.models import Message
from one_time_passwords.models import OneTimePassword
from orders.models import Order
from patterns.models import Pattern
from payment_methods.models import PaymentMethod
from plants.models import Plant
from promo_code.models import PromoCode
from receipts.models import Receipt
from reservations.models import Reservation
from reservations_types.models import ReservationType
from services.models import Service
from services_prices.models import ServicesPrice
from sessions.models import Session
from slots.models import Slot
from slots_types.models import SlotsType
from transactions.models import Transaction
from users.models import User
from users_companies_settings.models import UsersCompaniesSetting
from users_groups.models import UsersGroup
from datetime import datetime

types = {'int': 1, 'str': 'test', 'dec': 0.0, 'float': 0.0, 'bool': True, 'date': datetime.now(),
         'json': {}, 'array_int': [0, 1, 2], 'array_str': ['a', 'b', 'c']}


class TestData:
    model = None
    data = {}
    fk = {}
    row = None

    def insert(self):
        if self.model:
            record = self.model(**self.data)
            db_session.add(record)
            db_session.commit()
            db_session.refresh(record)
            self.row = record

    def clean_fk(self):
        if len(self.fk) == 0:
            return
        t = self.model
        for key, val in self.fk.items():
            self.fk[key] = None
        db_session.query(t).update(self.fk)
        db_session.commit()


class TestAccount(TestData):
    def __init__(self):
        self.model = Account
        self.data = dict(item_type='user',
                         balance=types['int'],
                         reserved=types['int'],
                         available=types['int'])


class TestBooking(TestData):
    def __init__(self):
        self.model = Booking
        self.fk = {
            'parent': None,
            'company_id': None,
            'added_user': None,
            'user_id': None,
            'activate_user': None,
            'canceled_user': None,
            'edited_user': None,
            'fiscal_receipt_printed_user': None,
            'service_id': None,
            'plant_id': None,
            'order_id': None,
            'payment_method': None,
            'cart_id': None
        }
        self.data = dict(quantity=types['int'],
                         discount=types['int'],
                         number=types['int'],
                         price=types['int'],
                         sum=types['int'],
                         receipts_used=types['int'],
                         receipts_printed=types['int'],
                         refund_sum=types['dec'],
                         fee_sum=types['dec'],
                         agent_sum=types['dec'],
                         company_sum=types['dec'],
                         fee_com=types['float'],
                         customer_first_name=types['str'],
                         customer_second_name=types['str'],
                         service_name=types['str'],
                         status='Not_Activated',
                         phone_number=types['str'],
                         user_first_name=types['str'],
                         user_last_name=types['str'],
                         comment=types['str'],
                         order_number=types['str'],
                         is_ordered=types['bool'],
                         added_date=types['date'],
                         target_time=types['date'],
                         activate_date=types['date'],
                         edited_date=types['date'],
                         completed_date=types['date'],
                         payed_date=types['date'],
                         canceled_date=types['date'],
                         fiscal_receipt_printed_date=types['date'],
                         properties=types['json'])


class TestCart(TestData):
    def __init__(self):
        self.model = Cart
        self.fk = {
            'company_id': None,
            'user_id': None,
            'added_user': None,
            'edited_user': None,
            'payment_method': None
        }
        self.data = dict(bookings=types['array_int'],
                         added_date=types['date'],
                         edited_date=types['date'],
                         expiration_date=types['date'],
                         status='added',
                         sum=types['dec'])


class TestCompany(TestData):
    def __init__(self):
        self.model = Company
        self.fk = {'default_user_id': None}
        self.data = dict(name=types['str'],
                         city=types['str'],
                         booking_phones=types['array_str'],
                         status=1,
                         site=types['str'],
                         acquiring_comission=types['float'],
                         agent_comission=types['float'],
                         refund_comission=types['json'],
                         terms=types['json'],
                         custom_domain=types['str'],
                         messages_templates=types['json'],
                         share_settings=types['json'],
                         key=types['str'],
                         reservation_slot_type_id=types['int'],
                         public_booking_payment_types=types['array_int'],
                         enabled_modules=types['json'],
                         third_party_auth_settings=types['json'],
                         sms_delivery_settings=types['json'])


class TestLoginLog(TestData):
    def __init__(self):
        self.model = LoginLog
        self.fk = {'user_id': None}
        self.data = dict(auth_method='cookies',
                         phone=types['int'],
                         added_date=types['date'],
                         login_by='login',
                         successful=types['bool'])


class TestLog(TestData):
    def __init__(self):
        self.model = Log
        self.fk = {
            'added_user': None,
            'session_id': None
        }
        self.data = dict(added_date=types['date'],
                         request=types['json'],
                         response=types['json'],
                         url=types['str'],
                         level='log',
                         exception=types['str'])


class TestMessage(TestData):
    def __init__(self):
        self.model = Message
        self.fk = {'user_id': None}
        self.data = dict(message=types['str'],
                         delivery_address=types['str'],
                         delivery_method='sms',
                         event_type='oneTimePassword',
                         status='queued',
                         sum=types['dec'])


class TestOneTimePassword(TestData):
    def __init__(self):
        self.model = OneTimePassword
        self.fk = {'user_id': None}
        self.data = dict(value=types['str'],
                         added_date=types['date'],
                         login_date=types['date'],
                         expiration_date=types['date'],
                         is_used=types['bool'])


class TestOrder(TestData):
    def __init__(self):
        self.model = Order
        self.fk = {
            'added_user': None,
            'edited_user': None,
            'payment_method': None,
            'cart_id': None
        }
        self.data = dict(number=types['str'],
                         status='ordered',
                         sum=types['dec'],
                         refund_sum=types['dec'],
                         fee_sum=types['dec'],
                         payed_sum=types['dec'],
                         added_date=types['date'],
                         edited_date=types['date'],
                         payed_date=types['date'])


class TestPattern(TestData):
    def __init__(self):
        self.model = Pattern
        self.fk = {'company_id' : None}
        self.data = dict(value=types['str'])


class TestPaymentMethod(TestData):
    def __init__(self):
        self.model = PaymentMethod
        self.data = dict(name=types['str'],
                         letter=types['str'],
                         public_name=types['str'],
                         print_fiscal_receipt=types['bool'])


class TestPlant(TestData):
    def __init__(self):
        self.model = Plant
        self.fk = {'company_id': None}
        self.data = dict(name=types['str'],
                         timeZone=types['str'],
                         timeZoneOffset=types['int'],
                         weekSchedule=types['str'])


class TestPromoCode(TestData):
    def __init__(self):
        self.model = PromoCode
        self.fk = {'company_id': None}
        self.data = dict(code=types['str'],
                         used=types['int'],
                         limit=types['int'],
                         valid_from=types['date'],
                         valid_to=types['date'])


class TestReceipt(TestData):
    def __init__(self):
        self.model = Receipt
        self.fk = {
            'booking_id': None,
            'added_user': None
        }
        self.data = dict(booking_number=types['int'],
                         number=types['int'],
                         added_date=types['date'])


class TestReservation(TestData):
    def __init__(self):
        self.model = Reservation
        self.fk = {'plant_id': None}
        self.data = dict(target_time=types['date'],
                         duration=types['date'],
                         public_name=types['str'],
                         type=types['str'])


class TestReservationType(TestData):
    def __init__(self):
        self.model = ReservationType
        self.fk = {'company_id': None}
        self.data = dict(name=types['str'])


class TestService(TestData):
    def __init__(self):
        self.model = Service
        self.fk = {'company_id': None}
        self.data = dict(name=types['str'],
                         booking_days_ahead=types['int'],
                         limit=types['int'],
                         public_limit=types['int'],
                         booking_with_another_services=types['bool'],
                         receipt_template=types['str'],
                         letter=types['str'],
                         plants=types['array_int'],
                         public_booking=types['bool'],
                         terms_short=types['str'],
                         order=types['int'],
                         display_column=types['bool'])


class TestServicesPrice(TestData):
    def __init__(self):
        self.model = ServicesPrice
        self.data = dict(date_from=types['date'],
                         date_to=types['date'],
                         pattern_id=types['int'],
                         service_id=types['int'],
                         prices=types['str'])


class TestSession(TestData):
    def __init__(self):
        self.model = Session
        self.fk = {'user_id': None}
        self.data = dict(ssid=types['str'],
                         auth_method='Cookies',
                         added_date=types['date'],
                         expiration_date=types['date'],
                         is_active=types['bool'],
                         logout_date=types['date'])


class TestSlot(TestData):
    def __init__(self):
        self.model = Slot
        self.fk = {'plant_id': None}
        self.data = dict(date_time=types['date'],
                         duration=types['date'],
                         public_comment=types['str'],
                         available=types['bool'],
                         capacity=types['int'],
                         public_limit=types['int'],
                         number=types['int'],
                         numeration_type=types['str'],
                         type=types['str'],
                         services_count=types['int'])


class TestSlotsType(TestData):
    def __init__(self):
        self.model = SlotsType
        self.fk = {'company_id': None}
        self.data = dict(name=types['str'],
                         description=types['str'],
                         is_public=types['bool'],
                         letter=types['str'])


class TestTransaction(TestData):
    def __init__(self):
        self.model = Transaction
        self.fk = {
            'from_account_id': None,
            'to_account_id': None,
            'company_id': None,
            'order_id': None,
            'added_user': None
        }
        self.data = dict(type='acquiring',
                         from_item_id=types['int'],
                         from_item_type='user',
                         to_item_id=types['int'],
                         to_item_type='user',
                         added_date=types['date'],
                         status='completed',
                         sum=types['dec'])


class TestUser(TestData):
    def __init__(self):
        self.model = User
        self.fk = {
            'registered_by_company': None,
            'promo_code_id': None
        }
        self.data = dict(first_name=types['str'],
                         last_name=types['str'],
                         middle_name=types['str'],
                         gender='Male',
                         phone=types['str'],
                         email=types['str'],
                         birthday=types['date'],
                         user_agreement=types['date'],
                         password=types['str'],
                         registered_date=types['date'],
                         photo=types['bool'],
                         roles='User',
                         registered_by_type=0,
                         last_login_date=types['date'])


class TestUserCompaniesSetting(TestData):
    def __init__(self):
        self.model = UsersCompaniesSetting
        self.fk = {
            'company_id': None,
            'user_id': None
        }
        self.data = dict(roles=['Customer'],
                         card_number=types['int'],
                         has_card=types['bool'],
                         card_issued_date=types['date'],
                         note=types['str'],
                         status=types['str'],
                         terms_date=types['date'],
                         is_blocked=types['bool'],
                         is_subscribed=types['bool'],
                         third_party_auth_ids=types['str'],
                         wasted=types['dec'],
                         wasted_with_card=types['dec'],
                         wasted_shift=types['dec'],
                         children_names=types['str'],
                         backoffice_print_receipt=types['bool'],
                         backoffice_plant_id=types['int'],
                         backoffice_offer_id=types['int'],
                         public_plant_id=types['int'],
                         public_service_id=types['int'],
                         public_slot_type_id=types['int'],
                         public_quantity=types['int'],
                         display_slots_only_working_hours=types['bool'])


class TestUserGroup(TestData):
    def __init__(self):
        self.model = UsersGroup
        self.fk = {'company_id': None}
        self.data = dict(name=types['str'],
                         description=types['str'],
                         total=types['int'])


class Data:
    def __init__(self):
        self.tables = {'accounts': TestAccount(),
                       'bookings': TestBooking(),
                       'carts': TestCart(),
                       'companies': TestCompany(),
                       'login_logs': TestLoginLog(),
                       'logs': TestLog(),
                       'messages': TestMessage(),
                       'one_time_passwords': TestOneTimePassword(),
                       'orders': TestOrder(),
                       'patterns': TestPattern(),
                       'payment_methods': TestPaymentMethod(),
                       'plants': TestPlant(),
                       'promo_codes': TestPromoCode(),
                       'receipts': TestReceipt(),
                       'reservations': TestReservation(),
                       'reservation_types': TestReservationType(),
                       'services': TestService(),
                       'service_prices': TestServicesPrice(),
                       'sessions': TestSession(),
                       'slots': TestSlot(),
                       'slot_types': TestSlotsType(),
                       'transactions': TestTransaction(),
                       'users': TestUser(),
                       'user_companies_settings': TestUserCompaniesSetting(),
                       'user_groups': TestUserGroup()}

    def fill_db(self):
        for name, table in self.tables.items():
            table.insert()
        self.fill_fk()

    def fill_fk(self):
        t = self.tables
        t['transactions'].row.from_account_id = t['accounts'].row.id
        t['transactions'].row.to_account_id = t['accounts'].row.id

        t['bookings'].row.parent = t['bookings'].row.id
        t['receipts'].row.booking_id = t['bookings'].row.id

        t['bookings'].row.company_id = t['companies'].row.id
        t['carts'].row.company_id = t['companies'].row.id
        t['patterns'].row.company_id = t['companies'].row.id
        t['plants'].row.company_id = t['companies'].row.id
        t['promo_codes'].row.company_id = t['companies'].row.id
        t['reservation_types'].row.company_id = t['companies'].row.id
        t['services'].row.company_id = t['companies'].row.id
        t['slot_types'].row.company_id = t['companies'].row.id
        t['transactions'].row.company_id = t['companies'].row.id
        t['users'].row.registered_by_company = t['companies'].row.id
        t['user_companies_settings'].row.company_id = t['companies'].row.id
        t['user_groups'].row.company_id = t['companies'].row.id

        t['bookings'].row.added_user = t['users'].row.id
        t['bookings'].row.user_id = t['users'].row.id
        t['bookings'].row.activate_user = t['users'].row.id
        t['bookings'].row.canceled_user = t['users'].row.id
        t['bookings'].row.edited_user = t['users'].row.id
        t['bookings'].row.fiscal_receipt_printed_user = t['users'].row.id
        t['carts'].row.user_id = t['users'].row.id
        t['carts'].row.added_user = t['users'].row.id
        t['carts'].row.edited_user = t['users'].row.id
        t['companies'].row.default_user_id = t['users'].row.id
        t['login_logs'].row.user_id = t['users'].row.id
        t['logs'].row.added_user = t['users'].row.id
        t['messages'].row.user_id = t['users'].row.id
        t['one_time_passwords'].row.user_id = t['users'].row.id
        t['orders'].row.added_user = t['users'].row.id
        t['orders'].row.edited_user = t['users'].row.id
        t['receipts'].row.added_user = t['users'].row.id
        t['sessions'].row.user_id = t['users'].row.id
        t['transactions'].row.added_user = t['users'].row.id
        t['user_companies_settings'].row.user_id = t['users'].row.id

        t['bookings'].row.service_id = t['services'].row.id
        t['logs'].row.session_id = t['sessions'].row.id

        t['bookings'].row.plant_id = t['plants'].row.id
        t['reservations'].row.plant_id = t['plants'].row.id
        t['slots'].row.plant_id = t['plants'].row.id

        t['bookings'].row.payment_method = t['payment_methods'].row.id
        t['carts'].row.payment_method = t['payment_methods'].row.id
        t['orders'].row.payment_method = t['payment_methods'].row.id

        t['users'].row.promo_code_id = t['promo_codes'].row.id

        t['bookings'].row.cart_id = t['carts'].row.id
        t['orders'].row.cart_id = t['carts'].row.id

        t['bookings'].row.order_id = t['orders'].row.id
        t['transactions'].row.order_id = t['orders'].row.id
        db_session.commit()

    def clean_db(self):
        for name, obj in self.tables.items():
            obj.clean_fk()
        for name, obj in self.tables.items():
            db_session.query(obj.model).delete()
            db_session.commit()
