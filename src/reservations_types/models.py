from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey


class ReservationType(Base):
    __tablename__ = 'reservation_types'
    id = Column(Integer, primary_key=True)
    company_id = Column(Integer, ForeignKey('companies.id'))
    name = Column(String)

    def __init__(self, **kwargs):
        self.company_id = kwargs.get('company_id')
        self.name = kwargs.get('name')
