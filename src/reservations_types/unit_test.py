from reservations_types.models import ReservationType
from lib.tests import AvendaTestCase


class ReservationTypeTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.data = {'company_id': self.comp.id}
        self.link = '/api/v2/reservations_types/'
        self._items_to_delete = [self.comp, self.user]
        self.model = ReservationType

    def test_db(self):
        self.db(self.model, self.data)
