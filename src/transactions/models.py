from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, DECIMAL
from enum import Enum
from sqlalchemy.orm import validates

from lib.validators import validate_value_in_enum


class ToItemType(Enum):

    BANK = 'bank'
    COMPANY = 'company'
    USER = 'user'
    AVENDA = 'avenda'


class FromItemType(Enum):

    BANK = 'bank'
    COMPANY = 'company'
    USER = 'user'
    AVENDA = 'avenda'


class Type(Enum):

    ACQUIRING = 'acquiring'
    CHARGE = 'charge'
    PURCHASE = 'purchase'
    REFUND = 'refund'
    WITHDRAWAL = 'withdrawal'
    UNDOREFUND = 'undoRefund'


class Status(Enum):

    ERROR = 'error'
    COMPLETED = 'completed'


class Transaction(Base):
    __tablename__ = 'transactions'
    id = Column(Integer, primary_key=True)
    type = Column(String)
    from_account_id = Column(Integer, ForeignKey('accounts.id'))
    from_item_id = Column(Integer)
    from_item_type = Column(String)
    to_account_id = Column(Integer, ForeignKey('accounts.id'))
    to_item_id = Column(Integer)
    to_item_type = Column(String)
    company_id = Column(Integer, ForeignKey('companies.id'))
    order_id = Column(Integer, ForeignKey('orders.id'))
    added_date = Column(DateTime)
    added_user = Column(Integer, ForeignKey('users.id'))
    status = Column(String)
    sum = Column(DECIMAL, default=0.0)

    def __init__(self, **kwargs):
        self.type = kwargs.get('type')
        self.from_account_id = kwargs.get('from_account_id')
        self.from_item_id = kwargs.get('from_item_id')
        self.from_item_type = kwargs.get('from_item_type')
        self.to_account_id = kwargs.get('to_account_id')
        self.to_item_id = kwargs.get('to_item_id')
        self.to_item_type = kwargs.get('to_item_type')
        self.company_id = kwargs.get('company_id')
        self.order_id = kwargs.get('order_id')
        self.added_date = kwargs.get('added_date')
        self.added_user = kwargs.get('added_user')
        self.status = kwargs.get('status')
        self.sum = kwargs.get('sum')

    @validates('status')
    def validates_status(self, key, status):
        validate_value_in_enum(status, Status)

    @validates('type')
    def validates_type(self, key, value):
        return validate_value_in_enum(value, Type)

    @validates('from_item_type')
    def validates_from_item_type(self, key, value):
        return validate_value_in_enum(value, FromItemType)

    @validates('to_item_type')
    def validates_to_item_type(self, key, value):
        return validate_value_in_enum(value, ToItemType)
