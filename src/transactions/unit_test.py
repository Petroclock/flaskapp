from transactions.models import Transaction
from lib.tests import AvendaTestCase


class TransactionTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.account = self.create_account(self.user.id, self.comp.id)
        self.payment = self.create_payment_method()
        self.service = self.create_service(self.comp.id)
        self.plant = self.create_plant(self.comp.id)
        self.book = self.create_book(self.comp.id, self.user.id, self.service.id, self.plant.id)
        self.cart = self.create_cart(self.user.id, self.comp.id, self.book.id, self.payment.id)
        self.order = self.create_order(self.user.id, self.cart.id, self.payment.id)
        self.data = {'added_user': self.user.id, 'company_id': self.comp.id, 'order_id': self.order.id,
                     'from_account_id': self.account.id, 'to_account_id': self.account.id}
        self.link = '/api/v2/transactions/'
        self._items_to_delete = [self.order,
                                 self.cart,
                                 self.book,
                                 self.plant,
                                 self.service,
                                 self.payment,
                                 self.account,
                                 self.comp,
                                 self.user]
        self.model = Transaction

    def test_db(self):
        self.db(self.model, self.data)
