from flask import request
from lib.request import RequestParams
from .blueprint import companies_app
from sqlalchemy import text
from database import engine as connection
from lib.helpers import json_success, json_fail, parse_query_result
from lib.decorators import json_response
from users_companies_settings.models import UsersCompaniesSetting
import datetime
from database import db_session
from sqlalchemy.orm.exc import NoResultFound


@companies_app.route('/', methods=['POST'], endpoint='company')
@json_response
def company():
    try:
        data = RequestParams(request)
    except TypeError:
        return json_fail('Empty request')
    sql = data.sql_expression('companies')
    result = connection.execute(text(sql))
    response = parse_query_result(result)
    return json_success(response, len(response))


@companies_app.route('/accept-terms', methods=['POST'], endpoint='accept_terms')
@json_response
def accept_terms():
    try:
        data = RequestParams(request)
    except TypeError:
        return json_fail('Empty request')
    comp_id = data.data['company_id']
    condition = data.data['acceptTerms']
    if condition:
        date = datetime.datetime.now()
    else:
        date = None
    try:
        row = UsersCompaniesSetting.query.filter(UsersCompaniesSetting.company_id == comp_id).one()
    except NoResultFound:
        row = UsersCompaniesSetting(company_id=comp_id)
        db_session.add(row)
    if row.terms_date is None:
        row.terms_date = date
    db_session.commit()
    return json_success({}, count=0)
