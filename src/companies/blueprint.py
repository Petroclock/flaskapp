from flask import Blueprint
from .models import Company


companies_app = Blueprint('Companies', __name__)
from .views import company, accept_terms
