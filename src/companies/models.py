from database import Base
from sqlalchemy import Column, Integer, String, ARRAY, JSON, Float, ForeignKey
from enum import Enum
from sqlalchemy.orm import validates


Status = Enum('Status', 'Active Deleted Hidden')


class Company(Base):
    __tablename__ = 'companies'
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    city = Column(String(255))
    booking_phones = Column(ARRAY(String))
    status = Column(Integer, default=1)
    site = Column(String(255))
    acquiring_comission = Column(Float)
    agent_comission = Column(Float)
    refund_comission = Column(JSON)
    terms = Column(JSON)
    custom_domain = Column(String(255))
    messages_templates = Column(JSON)
    share_settings = Column(JSON)
    key = Column(String(255))
    reservation_slot_type_id = Column(Integer)
    default_user_id = Column(Integer, ForeignKey('users.id'), default=None)
    public_booking_payment_types = Column(ARRAY(Integer))
    enabled_modules = Column(JSON)
    third_party_auth_settings = Column(JSON)
    sms_delivery_settings = Column(JSON)

    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.city = kwargs.get('city')
        self.booking_phones = kwargs.get('booking_phones')
        self.status = kwargs.get('status')
        self.site = kwargs.get('site')
        self.acquiring_comission = kwargs.get('acquiring_comission')
        self.agent_comission = kwargs.get('agent_comission')
        self.refund_comission = kwargs.get('refund_comission')
        self.terms = kwargs.get('terms')
        self.custom_domain = kwargs.get('custom_domain')
        self.messages_templates = kwargs.get('messages_templates')
        self.share_settings = kwargs.get('share_settings')
        self.key = kwargs.get('key')
        self.reservation_slot_type_id = kwargs.get('reservation_slot_type_id')
        self.default_user_id = kwargs.get('default_user_id')
        self.public_booking_payment_types = kwargs.get('public_booking_payment_types')
        self.enabled_modules = kwargs.get('enabled_modules')
        self.third_party_auth_settings = kwargs.get('third_party_auth_settings')
        self.sms_delivery_settings = kwargs.get('sms_delivery_settings')

    @validates('status')
    def validate_status(self, key, status):
        for item in Status:
            if status == item.name:
                status = item.value
                return status
        return Status.Active.value
