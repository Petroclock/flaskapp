import unittest
from companies.models import Company
from users_companies_settings.models import UsersCompaniesSetting
from database import db_session
from lib.tests import AvendaTestCase
from flaskr import app
import json
from lib.helpers import ResponseStatus


class CompanyTest(AvendaTestCase):
    def setUp(self):
        self.app = app.test_client()
        self.user = self.create_user()
        self.data = {'name': 'test', 'default_user_id': self.user.id}
        self.link = '/api/v2/companies/'
        self.model = Company

    def test_db(self):
        self.db(self.model, self.data)

    def test_view_success(self):
        company = self.get_or_create_company(self.user.id)
        data = {'filters': {'limit': 10,
                            'conditions': []},
                'data': {}}
        self.view_success(data, company.id, self.user.id)
        data['filters']['conditions'].append({
            "field": "default_user_id",
            "condition": "=",
            "value": self.user.id
        })
        self.view_success(data, company.id, self.user.id)
        self.delete_row(company)

    def test_view_fail(self):
        self.empty_request(self.link)

    def view_success(self, data, comp_id, user_id):
        response = self.request_success(self.link, data)
        self.assertEqual(response['total'], 1)
        self.assertEqual(len(response['data']), 1)
        self.assertEqual(response['data'][0]['id'], comp_id)
        self.assertEqual(response['data'][0]['default_user_id'], user_id)

    def test_terms(self):
        comp = self.get_or_create_company(self.user.id)
        # setting = self.create_test_user_setting(comp.id, self.user.id)
        db_session.add(comp)
        db_session.refresh(comp)
        self.terms_fail()
        row = None
        for item in [False, True]:
            data = {'filters': {'limit': 10,
                                'conditions': []},
                    'data': {'company_id': comp.id, 'acceptTerms': item}}
            request = self.terms_request(data)
            row = UsersCompaniesSetting.query.filter(
                UsersCompaniesSetting.company_id == data['data']['company_id']).first()
            self.terms_success(row, request, data)
        self.delete_row(row)
        self.delete_row(comp)

    def terms_request(self, data):
        result = self.app.open(path=self.link + 'accept-terms', method='POST', content_type='application/json',
                               data=json.dumps(data))
        response = json.loads(result.data.decode('utf8'))
        return response

    def terms_success(self, row, request, data):
        db_session.add(row)
        self.assertIsInstance(row, UsersCompaniesSetting)
        self.assertEqual(row.company_id, data['data']['company_id'])
        if data['data']['acceptTerms']:
            self.assertIsNotNone(row.terms_date)
        else:
            self.assertIsNone(row.terms_date)
        self.assertEqual(request['result'], ResponseStatus.SUCCESS.value)

    def terms_fail(self):
        result = self.app.open(path=self.link + 'accept-terms', method='POST')
        response = json.loads(result.data.decode('utf8'))
        self.assertEqual(response['result'], ResponseStatus.FAIL.value)
        self.assertEqual(response['data'], None)

    def tearDown(self):
        self.delete_row(self.user)


if __name__ == '__main__':
    unittest.main()
