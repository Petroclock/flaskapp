from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey


class Pattern(Base):
    __tablename__ = 'patterns'
    id = Column(Integer, primary_key=True)
    company_id = Column(Integer, ForeignKey('companies.id'))
    value = Column(String, default='{}')

    def __init__(self, **kwargs):
        self.company_id = kwargs.get('company_id')
        self.value = kwargs.get('value')
