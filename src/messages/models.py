from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey, DECIMAL
from enum import Enum
from sqlalchemy.orm import validates

from lib.validators import validate_value_in_enum


class DeliveryMethod(Enum):

    SMS = 'sms'
    EMAIL = 'email'


class Status(Enum):

    QUEUED = 'queued'
    DELIVERED = 'delivered'
    ERROR = 'error'
    CANCELLED = 'cancelled'


class EventType(Enum):

    ONETIMEPASSWORD = 'oneTimePassword'
    REGISTRATION = 'registration'
    SUBSCRIPTION = 'subscription'
    BIRTHDAY = 'birthday'


class Message(Base):
    __tablename__ = 'messages'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    message = Column(String)
    delivery_address = Column(String)
    delivery_method = Column(String)
    event_type = Column(String)
    status = Column(String)
    sum = Column(DECIMAL, default=0.0)

    def __init__(self, **kwargs):
        self.user_id = kwargs.get('user_id')
        self.message = kwargs.get('message')
        self.delivery_address = kwargs.get('delivery_address')
        self.delivery_method = kwargs.get('delivery_method')
        self.event_type = kwargs.get('event_type')
        self.status = kwargs.get('status')
        self.sum = kwargs.get('sum')

    @validates('status')
    def validates_status(self, key, status):
        validate_value_in_enum(status, Status)

    @validates('delivery_method')
    def validates_delivery_method(self, key, value):
        return validate_value_in_enum(value, DeliveryMethod)

    @validates('event_type')
    def validates_delivery_method(self, key, value):
        return validate_value_in_enum(value, EventType)
