from messages.models import Message
from lib.tests import AvendaTestCase


class MessageTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.data = {'user_id': self.user.id}
        self.link = '/api/v2/messages/'
        self._items_to_delete = [self.user]
        self.model = Message

    def test_db(self):
        self.db(self.model, self.data)
