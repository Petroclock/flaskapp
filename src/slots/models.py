from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, DateTime
from plants.models import Plant


class Slot(Base):
    __tablename__ = 'slots'
    id = Column(Integer, primary_key=True)
    plant_id = Column(Integer, ForeignKey(Plant.id))
    date_time = Column(DateTime)
    duration = Column(DateTime)
    public_comment = Column(String)
    available = Column(Boolean)
    capacity = Column(Integer)
    public_limit = Column(Integer)
    number = Column(Integer)
    numeration_type = Column(String)
    type = Column(String)
    services_count = Column(Integer)

    def __init__(self, **kwargs):
        self.plant_id = kwargs.get('plant_id')
        self.date_time = kwargs.get('date_time')
        self.duration = kwargs.get('duration')
        self.public_comment = kwargs.get('public_comment')
        self.available = kwargs.get('available')
        self.capacity = kwargs.get('capacity')
        self.public_limit = kwargs.get('public_limit')
        self.number = kwargs.get('number')
        self.numeration_type = kwargs.get('numeration_type')
        self.type = kwargs.get('type')
        self.services_count = kwargs.get('services_count')
