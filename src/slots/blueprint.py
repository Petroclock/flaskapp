from flask import Blueprint
from slots.models import Slot


slots_app = Blueprint('Slots', __name__)
from .views import slot
