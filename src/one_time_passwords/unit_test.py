from .models import OneTimePassword
from lib.tests import AvendaTestCase
import datetime


class OneTimePassTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        now = datetime.datetime.now()
        self.data = dict(value='test', user_id=self.user.id, added_date=now,
                         login_date=now, expiration_date=now, is_used=True)
        self.model = OneTimePassword

    def test_db(self):
        self.db(self.model, self.data)

    def tearDown(self):
        self.delete_row(self.user)
