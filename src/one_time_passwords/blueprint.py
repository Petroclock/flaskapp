from flask import Blueprint
from one_time_passwords.models import OneTimePassword

one_time_passwords_app = Blueprint('one_time_passwords', __name__)
