from database import Base
from sqlalchemy import Column, Integer, Boolean, DateTime, String, ForeignKey
import datetime
import app_config


def expiration_date_default(added_date):
    if not isinstance(added_date, datetime.datetime):
        added_date = datetime.datetime.now()
    return added_date + app_config.ONE_TIME_PASSWORD_EXPIRATION_TIME_SPAN


class OneTimePassword(Base):

    __tablename__ = 'one_time_passwords'
    id = Column(Integer, primary_key=True)
    value = Column(String(255))
    from users.models import User
    user_id = Column(Integer, ForeignKey(User.id))
    added_date = Column(DateTime, default=datetime.datetime.now())
    login_date = Column(DateTime)
    expiration_date = Column(DateTime, default=expiration_date_default(added_date))
    is_used = Column(Boolean)

    def __init__(self, **kwargs):
        self.value = kwargs.get('value')
        self.user_id = kwargs.get('user_id')
        self.added_date = kwargs.get('added_date')
        self.login_date = kwargs.get('login_date')
        self.expiration_date = kwargs.get('expiration_date')
        self.is_used = kwargs.get('is_used')
