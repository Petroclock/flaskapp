import unittest
from .models import LoginLog
from lib.tests import AvendaTestCase
import datetime


class LogsTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.model = LoginLog
        self.data = {'auth_method': 'token',
                     'phone': 89997776655,
                     'user_id': self.user.id,
                     'added_date': datetime.datetime.now(),
                     'login_by': 'login',
                     'successful': True}

    def test_db(self):
        self.db(self.model, self.data)

    def tearDown(self):
        self.delete_row(self.user)


if __name__ == '__main__':
    unittest.main()
