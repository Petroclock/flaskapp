from database import Base
from sqlalchemy import Column, Integer, Boolean, DateTime, String, ForeignKey, BigInteger
from enum import Enum
from sqlalchemy.orm import validates

AuthMethod = Enum('AuthMethod', 'cookies token')
LoginBy = Enum('LoginBy', 'login oneTimePassword vk facebook')


class LoginLog(Base):
    __tablename__ = 'login_logs'
    id = Column(Integer, primary_key=True)
    auth_method = Column(String(255))
    phone = Column(BigInteger)
    from users.models import User
    user_id = Column(Integer, ForeignKey(User.id))
    added_date = Column(DateTime)
    login_by = Column(String(255))
    successful = Column(Boolean)

    def __init__(self, **kwargs):
        self.auth_method = kwargs.get('auth_method')
        self.phone = kwargs.get('phone')
        self.user_id = kwargs.get('user_id')
        self.added_date = kwargs.get('added_date')
        self.login_by = kwargs.get('login_by')
        self.successful = kwargs.get('successful')

    @validates('auth_method')
    def validate_auth_method(self, key, method):
        valid_values = [item.name for item in AuthMethod]
        valid_values.append(None)
        assert method in valid_values
        return method

    @validates('login_by')
    def validate_login_by(self, key, login):
        valid_values = [item.name for item in LoginBy]
        valid_values.append(None)
        assert login in valid_values
        return login
