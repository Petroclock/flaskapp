from flask import Blueprint
from reservations.models import Reservation


reservations_app = Blueprint('Reservations', __name__)
from .views import reservation