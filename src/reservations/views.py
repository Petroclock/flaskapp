from flask import request
from lib.request import RequestParams
from .blueprint import reservations_app
from lib.helpers import json_success, json_fail, parse_query_result
from lib.decorators import json_response
from database import engine as connection
from sqlalchemy import text


@reservations_app.route('/', methods=['POST'], endpoint='reservation')
@json_response
def reservation():
    try:
        data = RequestParams(request)
    except TypeError:
        return json_fail('Empty request')
    sql = data.sql_expression('reservations')
    result = connection.execute(text(sql))
    response = parse_query_result(result)
    return json_success(response, len(response))
