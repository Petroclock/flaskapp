import unittest
from reservations.models import Reservation
from database import db_session
from lib.tests import AvendaTestCase


class ReservationTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.plant = self.create_plant(self.comp.id)
        self.data = {'plant_id': self.plant.id}
        self.link = '/api/v2/reservations/'
        self.model = Reservation
        self._items_to_delete = [self.plant, self.comp, self.user]

    def test_db(self):
        self.db(self.model, self.data)

    def test_view_success(self):
        record = self.create_insert(self.model, self.data)
        data = {'data': {}}
        response = self.request_success(self.link, data)
        for item in (self.user, self.comp, self.plant):
            db_session.add(item)
        self.assertEqual(response['total'], 1)
        self.assertEqual(len(response['data']), 1)
        self.assertEqual(response['data'][0]['id'], record.id)
        self.assertEqual(response['data'][0]['plant_id'], self.plant.id)
        self.delete_row(record)

    def test_view_fail(self):
        self.empty_request(self.link)

if __name__ == '__main__':
    unittest.main()
