from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from plants.models import Plant


class Reservation(Base):
    __tablename__ = 'reservations'
    id = Column(Integer, primary_key=True)
    plant_id = Column(Integer, ForeignKey(Plant.id))
    target_time = Column(DateTime)
    duration = Column(DateTime)
    public_name = Column(String)
    type = Column(String)

    def __init__(self, **kwargs):
        self.plant_id = kwargs.get('plant_id')
        self.target_time = kwargs.get('target_time')
        self.duration = kwargs.get('duration')
        self.public_name = kwargs.get('public_name')
        self.type = kwargs.get('type')
