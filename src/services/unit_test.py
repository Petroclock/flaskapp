import unittest
from .models import Service
from lib.tests import AvendaTestCase


class ServiceTest(AvendaTestCase):

    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.data = dict(company_id=self.comp.id, name='test', booking_days_ahead=1, booking_with_another_services=True,
                         limit=10, public_booking=True, public_limit=15, receipt_template='test', letter='test',
                         plants=['test'], terms_short='test', order=1, display_column=True)
        self._items_to_delete = [self.comp, self.user]
        self.model = Service

    def test_db(self):
        self.db(self.model, self.data)


if __name__ == '__main__':
    unittest.main()
