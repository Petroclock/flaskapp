from database import Base
from sqlalchemy import Column, Integer, Boolean, String, ForeignKey, ARRAY


class Service(Base):
    __tablename__ = 'services'
    id = Column(Integer, primary_key=True)
    from companies.models import Company
    company_id = Column(Integer, ForeignKey(Company.id))
    name = Column(String(255))
    booking_days_ahead = Column(Integer)
    limit = Column(Integer)
    public_limit = Column(Integer)
    booking_with_another_services = Column(Boolean)
    receipt_template = Column(String(255))
    letter = Column(String(255))
    plants = Column(ARRAY(String))
    public_booking = Column(Boolean)
    terms_short = Column(String(255))
    order = Column(Integer)
    display_column = Column(Boolean)

    def __init__(self, **kwargs):
        self.company_id = kwargs.get('company_id')
        self.name = kwargs.get('name')
        self.booking_days_ahead = kwargs.get('booking_days_ahead')
        self.limit = kwargs.get('limit')
        self.public_limit = kwargs.get('public_limit')
        self.booking_with_another_services = kwargs.get('booking_with_another_services')
        self.receipt_template = kwargs.get('receipt_template')
        self.letter = kwargs.get('letter')
        self.plants = kwargs.get('plants')
        self.public_booking = kwargs.get('public_booking')
        self.terms_short = kwargs.get('terms_short')
        self.order = kwargs.get('order')
        self.display_column = kwargs.get('display_column')
