# coding=utf-8;

import datetime

from flask import request
from sqlalchemy.orm.exc import NoResultFound

from lib.request import RequestParams
from lib.helpers import json_success, json_fail, get_user
from lib.decorators import json_response

from .blueprint import sessions_app

from users.models import User
from .models import Session


@sessions_app.route('/', methods=['POST'], endpoint='sessions')
@json_response
def sessions():
    try:
        data = RequestParams(request)
        user_id = get_user()
        Session.query.filter(Session.user_id == user_id,
                             Session.ssid == data.data['token'],
                             Session.auth_method == data.data['authMethod']).one()
        user = User.query.filter(User.id == user_id).one()
        return json_success({
            'userId': user.id,
            'phone': user.phone,
            'email': user.email,
            'firstName': user.first_name,
            'lastName': user.last_name,
            'middleName': user.middle_name,
            'birthday': user.birthday,
            'gender': user.gender,
            'roles': user.roles,
            'currentDateTime': datetime.datetime.now()
        })
    except TypeError:
        return json_fail('Empty request')
    except NoResultFound:
        return json_fail('Not found')
    except KeyError:
        return json_fail('Empty request: no keys')
    except AssertionError:
        return json_fail('No valid value')
