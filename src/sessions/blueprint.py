from flask import Blueprint
from .models import Session

sessions_app = Blueprint('Sessions', __name__)
from .views import sessions
