import unittest
from .models import Session
from lib.tests import AvendaTestCase
import datetime


class SessionTest(AvendaTestCase):

    def setUp(self):
        self.user = self.create_user()
        self.user.phone = '8800'
        self.user.password = 'test'
        now = datetime.datetime.now()
        self.data = dict(ssid='test',
                         auth_method='Cookies',
                         user_id=self.user.id,
                         added_date=now,
                         expiration_date=now,
                         is_active=True,
                         logout_date=now)
        self._items_to_delete = [self.user]
        self.model = Session
        self.link = '/api/v2/sessions/'

    def test_db(self):
        self.db(self.model, self.data)

    def test_view(self):
        session = self.create_insert(self.model, self.data)
        data = {'data': {'token': self.data['ssid'], 'authMethod': self.data['auth_method']}}
        with self.app:
            self.app.post('/login', data={'login': self.user.phone, 'password': self.user.password})
            response = self.request_success(self.link, data)
            self.app.get('/logout')
        check = {
            'userId': self.user.id,
            'phone': self.user.phone,
            'email': self.user.email,
            'firstName': self.user.first_name,
            'lastName': self.user.last_name,
            'middleName': self.user.middle_name,
            'birthday': self.user.birthday,
            'gender': self.user.gender,
            'roles': self.user.roles
        }
        for key, val in check.items():
            self.assertEqual(response['data'][key], val)
        self.delete_row(session)


if __name__ == '__main__':
    unittest.main()
