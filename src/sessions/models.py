from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Boolean
from enum import Enum
from sqlalchemy.orm import validates
from users.models import User


AuthMethod = Enum('AuthMethod', 'Cookies Token')


class Session(Base):
    __tablename__ = 'sessions'
    id = Column(Integer, primary_key=True)
    ssid = Column(String(255))
    auth_method = Column(String(255))
    user_id = Column(Integer, ForeignKey(User.id))
    added_date = Column(DateTime)
    expiration_date = Column(DateTime)
    is_active = Column(Boolean)
    logout_date = Column(DateTime)

    def __init__(self, **kwargs):
        self.ssid = kwargs.get('ssid')
        self.auth_method = kwargs.get('auth_method')
        self.user_id = kwargs.get('user_id')
        self.added_date = kwargs.get('added_date')
        self.expiration_date = kwargs.get('expiration_date')
        self.is_active = kwargs.get('is_active')
        self.logout_date = kwargs.get('logout_date')

    @validates('auth_method')
    def validate_auth_method(self, key, method):
        valid_values = [item.name for item in AuthMethod]
        valid_values.append(None)
        assert method in valid_values
        return method
