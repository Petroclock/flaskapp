from database import Base
from sqlalchemy import Column, Integer, DateTime, String, ForeignKey


class PromoCode(Base):
    __tablename__ = 'promo_codes'
    id = Column(Integer, primary_key=True)
    from companies.models import Company
    company_id = Column(Integer, ForeignKey(Company.id))
    code = Column(String(255))
    used = Column(Integer)
    limit = Column(Integer)
    valid_from = Column(DateTime)
    valid_to = Column(DateTime)

    def __init__(self, **kwargs):
        self.company_id = kwargs.get('company_id')
        self.code = kwargs.get('code')
        self.used = kwargs.get('used')
        self.limit = kwargs.get('limit')
        self.valid_from = kwargs.get('valid_from')
        self.valid_to = kwargs.get('valid_to')
