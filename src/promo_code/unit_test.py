import unittest
from .models import PromoCode
import datetime
from lib.tests import AvendaTestCase


class PromoCodeTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        now = datetime.datetime.now()
        self.data = dict(company_id=self.comp.id, code='111', used=1, limit=10,
                         valid_from=now, valid_to=now)
        self._items_to_delete = [self.comp, self.user]
        self.model = PromoCode

    def test_db(self):
        self.db(self.model, self.data)


if __name__ == '__main__':
    unittest.main()
