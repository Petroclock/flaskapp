import datetime
from enum import Enum

from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, DECIMAL
from sqlalchemy.orm import validates

from lib.validators import validate_value_in_enum


class Status(Enum):

    ORDERED = 'ordered'
    CANCELED = 'Canceled'


class Order(Base):

    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True)
    number = Column(String)
    status = Column(String)
    sum = Column(DECIMAL, default=0.0)
    refund_sum = Column(DECIMAL, default=0.0)
    fee_sum = Column(DECIMAL, default=0.0)
    payed_sum = Column(DECIMAL, default=0.0)
    added_date = Column(DateTime)
    edited_date = Column(DateTime)
    payed_date = Column(DateTime)
    added_user = Column(Integer, ForeignKey('users.id'))
    edited_user = Column(Integer, ForeignKey('users.id'))
    cart_id = Column(Integer, ForeignKey('carts.id'))
    payment_method = Column(Integer, ForeignKey('payment_methods.id'))

    def __init__(self, **kwargs):
        self.number = kwargs.get('number')
        self.status = kwargs.get('status')
        self.sum = kwargs.get('sum')
        self.refund_sum = kwargs.get('refund_sum')
        self.fee_sum = kwargs.get('fee_sum')
        self.payed_sum = kwargs.get('payed_sum')
        self.added_date = kwargs.get('added_date')
        self.edited_date = kwargs.get('edited_date')
        self.payed_date = kwargs.get('payed_date')
        self.added_user = kwargs.get('added_user')
        self.edited_user = kwargs.get('edited_user')
        self.cart_id = kwargs.get('cart_id')
        self.payment_method = kwargs.get('payment_method')

    @validates('status')
    def validates_status(self, key, status):
        return validate_value_in_enum(status, Status)

    @staticmethod
    def generate_number():
        # TODO
        return '123abc'

    @staticmethod
    def create_from_cart(cart, added_user=None, payment_method=None):
        return Order(number=Order.generate_number(),
                     added_user=added_user or cart.user_id,
                     sum=cart.sum,
                     added_date=datetime.datetime.now(),
                     cart_id=cart.id,
                     payment_method=payment_method)


