from orders.models import Order
from lib.tests import AvendaTestCase


class OrderTest(AvendaTestCase):

    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.service = self.create_service(self.comp.id)
        self.plant = self.create_plant(self.comp.id)
        self.book = self.create_book(self.comp.id, self.user.id, self.service.id, self.plant.id)
        self.payment = self.create_payment_method()
        self.cart = self.create_cart(self.user.id, self.comp.id, self.book.id, self.payment.id)
        self.data = {
            'added_user': self.user.id,
            'cart_id': self.cart.id,
            'payment_method': self.payment.id
        }
        self.link = '/api/v2/orders/'
        self._items_to_delete = [self.cart, self.payment, self.book, self.plant, self.service, self.comp, self.user]
        self.model = Order

    def test_db(self):
        self.db(self.model, self.data)
