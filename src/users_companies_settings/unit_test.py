import unittest
from .models import UsersCompaniesSetting
from lib.tests import AvendaTestCase
from database import db_session


class UsersSettingTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.group = self.create_group(self.comp)
        self.data = dict(user_id=self.user.id,
                         company_id=self.comp.id,
                         groups=[self.group.id])
        self.link = '/api/v2/users-companies-settings/'
        self._items_to_delete = [self.group, self.comp, self.user]
        self.model = UsersCompaniesSetting

    def test_db(self):
        self.db(self.model, self.data)

    def test_view_success(self):
        record = self.create_insert(self.model, self.data)
        data = {'filters': {'limit': 10,
                            'conditions': [{
                                "field": "company_id",
                                "condition": "=",
                                "value": self.comp.id
                            }
                            ]},
                'data': {}}
        response = self.request_success(self.link, data)
        for item in (self.user, self.comp):
            db_session.add(item)
        self.assertEqual(response['total'], 1)
        self.assertEqual(len(response['data']), 1)
        self.assertEqual(response['data'][0]['id'], record.id)
        self.assertEqual(response['data'][0]['user_id'], self.user.id)
        self.assertEqual(response['data'][0]['company_id'], self.comp.id)
        self.delete_row(record)

    def test_view_fail(self):
        self.empty_request(self.link)

if __name__ == '__main__':
    unittest.main()
