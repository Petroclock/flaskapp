from flask import Blueprint
from .models import UsersCompaniesSetting

users_companies_settings_app = Blueprint('Users_companies_settings', __name__)
from .views import user_setting
