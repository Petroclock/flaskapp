from flask import request
from lib.request import RequestParams
from .blueprint import users_companies_settings_app
from lib.helpers import json_success, json_fail, parse_query_result
from lib.decorators import json_response
from database import engine as connection
from sqlalchemy import text


@users_companies_settings_app.route('/', methods=['POST'], endpoint='user_setting')
@json_response
def user_setting():
    try:
        data = RequestParams(request)
    except TypeError:
        return json_fail('Empty request')
    sql = data.sql_expression('users_companies_settings')
    result = connection.execute(text(sql))
    response = parse_query_result(result)
    return json_success(response, len(response))
