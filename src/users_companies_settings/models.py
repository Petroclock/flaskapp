from database import Base, db_session
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Boolean, ARRAY, DECIMAL
from enum import Enum
from sqlalchemy.orm import validates
from users_groups.models import UsersGroup


Roles = Enum('Roles', 'Customer Operator Administrator Controller')


class UsersCompaniesSetting(Base):
    __tablename__ = 'users_companies_settings'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    company_id = Column(Integer, ForeignKey('companies.id'))
    roles = Column(ARRAY(String(255)), default=['Customer'])
    groups = Column(ARRAY(Integer))
    card_number = Column(Integer)
    has_card = Column(Boolean)
    card_issued_date = Column(DateTime)
    note = Column(String(255))
    status = Column(String(255), default='NotActivated')
    terms_date = Column(DateTime)
    is_blocked = Column(Boolean, default=False)
    is_subscribed = Column(Boolean, default=False)
    third_party_auth_ids = Column(String(255))
    wasted = Column(DECIMAL, default=0.0)
    wasted_with_card = Column(DECIMAL, default=0.0)
    wasted_shift = Column(DECIMAL, default=0.0)
    children_names = Column(String(255))
    backoffice_print_receipt = Column(Boolean, default=True)
    backoffice_plant_id = Column(Integer)
    backoffice_offer_id = Column(Integer)
    public_plant_id = Column(Integer)
    public_service_id = Column(Integer)
    public_slot_type_id = Column(Integer)
    public_quantity = Column(Integer)
    display_slots_only_working_hours = Column(Boolean, default=True)

    def __init__(self, **kwargs):
        self.user_id = kwargs.get('user_id')
        self.company_id = kwargs.get('company_id')
        self.roles = kwargs.get('roles')
        self.groups = kwargs.get('groups')
        self.card_number = kwargs.get('card_number')
        self.has_card = kwargs.get('has_card')
        self.card_issued_date = kwargs.get('card_issued_date')
        self.note = kwargs.get('note')
        self.status = kwargs.get('status')
        self.terms_date = kwargs.get('terms_date')
        self.is_blocked = kwargs.get('is_blocked')
        self.is_subscribed = kwargs.get('is_subscribed')
        self.third_party_auth_ids = kwargs.get('third_party_auth_ids')
        self.wasted = kwargs.get('wasted')
        self.wasted_with_card = kwargs.get('wasted_with_card')
        self.wasted_shift = kwargs.get('wasted_shift')
        self.children_names = kwargs.get('children_names')
        self.backoffice_print_receipt = kwargs.get('backoffice_print_receipt')
        self.backoffice_plant_id = kwargs.get('backoffice_plant_id')
        self.backoffice_offer_id = kwargs.get('backoffice_offer_id')
        self.public_plant_id = kwargs.get('public_plant_id')
        self.public_service_id = kwargs.get('public_service_id')
        self.public_slot_type_id = kwargs.get('public_slot_type_id')
        self.public_quantity = kwargs.get('public_quantity')
        self.display_slots_only_working_hours = kwargs.get('display_slots_only_working_hours')

    @validates('roles')
    def validate_roles(self, key, roles):
        if roles is None:
            return roles
        valid_roles = [item.name for item in Roles]
        if not isinstance(roles, list):
            roles = [roles]
        for role in roles:
            assert role in valid_roles
        return roles

    @validates('groups')
    def validate_groups(self, key, groups):
        if groups is None:
            return groups
        if isinstance(groups, int):
            groups = [groups]
        users_groups = [item.id for item in UsersGroup.query]
        for group in groups:
            assert group in users_groups
        return groups


def user_setting(data):
    if UsersCompaniesSetting.query.filter_by(user_id=data['user_id']).one_or_none():
        return
    setting = UsersCompaniesSetting(**data)
    db_session.add(setting)
    db_session.commit()
