from accounts.models import Account
from lib.tests import AvendaTestCase


class AccountTest(AvendaTestCase):

    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.data = {'user_id': self.user.id, 'company_id': self.comp.id}
        self.link = '/api/v2/accounts/'
        self._items_to_delete = [self.comp, self.user]
        self.model = Account

    def test_db(self):
        self.db(self.model, self.data)
