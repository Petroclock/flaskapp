from database import Base
from sqlalchemy import Column, Integer, String, ForeignKeyConstraint, DECIMAL
from enum import Enum
from sqlalchemy.orm import validates

from lib.validators import validate_value_in_enum


class Types(Enum):

    BANK = 'bank'
    COMPANY = 'company'
    USER = 'user'
    AVENDA = 'avenda'


class Account(Base):
    __tablename__ = 'accounts'
    id = Column(Integer, primary_key=True)
    item_id = ForeignKeyConstraint(['company_id', 'user_id'], ['companies.id', 'users.id'])
    item_type = Column(String)
    balance = Column(DECIMAL)
    reserved = Column(DECIMAL)
    available = Column(DECIMAL)

    def __init__(self, **kwargs):
        self.item_id = kwargs.get('item_id')
        self.item_type = kwargs.get('item_type')
        self.balance = kwargs.get('balance')
        self.reserved = kwargs.get('reserved')
        self.available = kwargs.get('available')

    @validates('item_type')
    def validates_item_type(self, key, item_type):
        validate_value_in_enum(item_type, Types)
