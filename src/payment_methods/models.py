from database import Base
from sqlalchemy import Column, Integer, String, Boolean


class PaymentMethod(Base):
    __tablename__ = 'payment_methods'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    letter = Column(String)
    public_name = Column(String)
    print_fiscal_receipt = Column(Boolean, default=False)

    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.letter = kwargs.get('letter')
        self.public_name = kwargs.get('public_name')
        self.print_fiscal_receipt = kwargs.get('print_fiscal_receipt')
