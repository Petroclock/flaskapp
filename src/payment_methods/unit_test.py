from payment_methods.models import PaymentMethod
from lib.tests import AvendaTestCase


class PaymentMethodTest(AvendaTestCase):
    def setUp(self):
        self.data = {}
        self.link = '/api/v2/payment_methods/'
        self.model = PaymentMethod

    def test_db(self):
        self.db(self.model, self.data)
