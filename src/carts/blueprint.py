from flask import Blueprint
from carts.models import Cart


carts_app = Blueprint('Carts', __name__)
from .views import cart, clean, delete, confirm_order
