from carts.models import Cart
from lib.tests import AvendaTestCase
from lib.helpers import get_user
from flaskr import db_session
from orders.models import Order
from bookings.models import Booking


class CartTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.user.phone = '8800'
        self.user.password = 'test'
        self.comp = self.get_or_create_company(self.user.id)
        self.service = self.create_service(self.comp.id)
        self.plant = self.create_plant(self.comp.id)
        self.payment_method = self.create_payment_method()
        self.data = {'user_id': self.user.id, 'company_id': self.comp.id, 'added_user': self.user.id,
                     'edited_user': self.user.id, 'payment_method': self.payment_method.id}
        self._items_to_delete = [self.payment_method, self.plant, self.service, self.comp, self.user]
        self.model = Cart
        self.view_link = '/api/v2/carts/'
        self.clean_link = self.view_link + 'clean'
        self.delete_link = self.view_link + 'delete'
        self.confirm_order_link = self.view_link + 'confirm-order'

    def test_db(self):
        self.db(self.model, self.data)

    def test_view(self):
        obj = self.create_insert(self.model, self.data)
        data = {'data': {'company_id': self.data['company_id']}}
        with self.app:
            self.app.post('/login', data={'login': self.user.phone, 'password': self.user.password})
            request = self.request_success(self.view_link, data)
            self.app.get('/logout')
        data = {'id': obj.id,
                'addedDate': obj.added_date.isoformat(),
                'addedUser': obj.added_user,
                'bookings': obj.bookings,
                'companyId': obj.company_id,
                'editedUser': obj.edited_user,
                'status': obj.status,
                'sum': obj.sum,
                'userId': obj.user_id}
        for key, val in data.items():
            self.assertEqual(request['data'][key], val)
        self.delete_row(obj)

    def test_clean(self):
        obj = self.create_insert(self.model, self.data)
        data = {'cart_id': obj.id, 'user_id': self.user.id}
        with self.app:
            self.app.post('/login', data={'login': self.user.phone, 'password': self.user.password})
            self.assertEqual(get_user(), self.user.id)
            self.request_success(self.clean_link, data)
            self.app.get('/logout')
        obj = self.model.query.filter(self.model.id == obj.id).one_or_none()
        self.assertIsNone(obj)

    def test_delete(self):
        book = self.create_book(self.comp.id, self.user.id, self.service.id, self.plant.id)
        cart = self.create_insert(self.model, self.data)
        cart.bookings = [book.id]
        db_session.commit()
        data = {'data': {'number': book.number}}
        with self.app:
            self.app.post('/login', data={'login': self.user.phone, 'password': self.user.password})
            self.request_success(self.delete_link, data)
            book = Booking.query.filter(Booking.id == book.id).one_or_none()
            db_session.add(cart)
            self.assertIsNone(book)
            self.assertEqual(len(cart.bookings), 0)
            self.delete_row(cart)
            self.app.get('/logout')

    def test_confirm_order(self):
        cart = self.create_insert(self.model, self.data)
        with self.app:
            self.app.post('/login', data={'login': self.user.phone, 'password': self.user.password})
            response = self.request_success(self.confirm_order_link, {})
            order = Order.query.filter(Order.number == response['data']['orderNumber']).one_or_none()
            self.assertIsNotNone(order)
            self.delete_row(order)
            self.delete_row(cart)
            self.app.get('/logout')
