from database import Base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, ARRAY, DECIMAL
from enum import Enum
from sqlalchemy.orm import validates
from datetime import datetime
from lib.helpers import get_user
import app_config

from lib.validators import validate_value_in_enum


def expiration_date_default(added_date):
    if not isinstance(added_date, datetime):
        added_date = datetime.now()
    return added_date + app_config.CART_LIFETIME


class Status(Enum):

    ADDED = 'added'
    ORDERED = 'ordered'
    EXPIRED = 'expired'


class Cart(Base):

    __tablename__ = 'carts'

    id = Column(Integer, primary_key=True)
    company_id = Column(Integer, ForeignKey('companies.id'))
    user_id = Column(Integer, ForeignKey('users.id'))
    bookings = Column(ARRAY(Integer))
    payment_method = Column(Integer, ForeignKey('payment_methods.id'))
    added_user = Column(Integer, ForeignKey('users.id'))
    edited_user = Column(Integer, ForeignKey('users.id'))
    added_date = Column(DateTime, default=datetime.now())
    edited_date = Column(DateTime)
    expiration_date = Column(DateTime, default=expiration_date_default(added_date))
    status = Column(String)
    sum = Column(DECIMAL)

    def __init__(self, **kwargs):
        self.company_id = kwargs.get('company_id')
        self.user_id = kwargs.get('user_id')
        self.bookings = kwargs.get('bookings')
        self.payment_method = kwargs.get('payment_method')
        self.added_user = kwargs.get('added_user')
        self.edited_user = kwargs.get('edited_user')
        self.added_date = kwargs.get('added_date')
        self.edited_date = kwargs.get('edited_date ')
        self.expiration_date = kwargs.get('expiration_date')
        self.status = kwargs.get('status')
        self.sum = kwargs.get('sum')

    @validates('status')
    def validates_status(self, key, status):
        return validate_value_in_enum(status, Status)

    @staticmethod
    def get_user_cart(user_id):
        return Cart.query(Cart.user_id == user_id).one()

    @staticmethod
    def get_cart():
        user_id = get_user()
        return Cart.get_user_cart(user_id=user_id)
