from flask import request
from sqlalchemy.orm.exc import NoResultFound
from lib.request import RequestParams
from .blueprint import carts_app
from lib.helpers import json_success, json_fail, get_user
from lib.decorators import json_response
from database import db_session

from bookings.models import Booking
from orders.models import Order
from .models import Cart


@carts_app.route('/clean', methods=['POST'], endpoint='clean')
@json_response
def clean():
    try:
        user_id = get_user()
        Cart.query.filter(Cart.user_id == user_id).delete()
        db_session.commit()
    except NoResultFound:
        return json_fail('Not found')
    except ValueError:
        return json_fail('No int value')
    except KeyError:
        return json_fail('Empty request')
    else:
        return json_success([])


@carts_app.route('/delete', methods=['POST'], endpoint='delete')
@json_response
def delete():
    try:
        data = RequestParams(request)
    except TypeError:
        return json_fail('Empty request')

    try:
        user_id = get_user()
        cart = Cart.query.filter(Cart.user_id == user_id).one()
        booking = Booking.query.filter(Booking.user_id == user_id,
                                       Booking.number == data.data['number']).one()
        booking_id = booking.id
        Booking.query.filter(Booking.id == booking_id).delete()
        cart.bookings = [b_id for b_id in cart.bookings if b_id != booking_id]
        db_session.commit()
    except NoResultFound:
        return json_fail('Not found')
    except ValueError:
        return json_fail('No int value')
    except KeyError:
        return json_fail('Empty request')
    else:
        return json_success([])


@carts_app.route('/', methods=['POST'], endpoint='cart')
@json_response
def cart():
    try:
        data = RequestParams(request)
    except TypeError:
        return json_fail('Empty request')

    try:
        cart = Cart.query.filter(Cart.company_id == data.data['company_id'],
                                 Cart.user_id == get_user()).one()

        return json_success({
            'id': cart.id,
            'addedDate': cart.added_date,
            'addedUser': cart.added_user,
            'bookings': cart.bookings,
            'companyId': cart.company_id,
            'editedUser': cart.edited_user,
            'status': cart.status,
            'sum': cart.sum,
            'userId': cart.user_id,
        })
    except NoResultFound:
        return json_fail('Not found')
    except ValueError:
        return json_fail('No int value')
    except KeyError:
        return json_fail('Empty request')


@carts_app.route('/confirm-order', methods=['POST'], endpoint='confirm_order')
@json_response
def confirm_order():
    try:
        user_id = get_user()
        cart = Cart.query.filter(Cart.user_id == user_id).one()
        order = Order.create_from_cart(cart)
        db_session.add(order)
        db_session.commit()
        return json_success({'orderNumber': order.number})
    except NoResultFound:
        return json_fail('Not found')
    except ValueError:
        return json_fail('No int value')
    except KeyError:
        return json_fail('Empty request')
