from database import Base
from sqlalchemy import Column, Integer, String, DateTime


class ServicesPrice(Base):
    __tablename__ = 'services_prices'
    id = Column(Integer, primary_key=True)
    date_from = Column(DateTime)
    date_to = Column(DateTime)
    pattern_id = Column(Integer)
    service_id = Column(Integer)
    prices = Column(String(255))

    def __init__(self, **kwargs):
        self.date_from = kwargs.get('date_from')
        self.date_to = kwargs.get('date_to')
        self.pattern_id = kwargs.get('pattern_id')
        self.service_id = kwargs.get('service_id')
        self.prices = kwargs.get('prices')
