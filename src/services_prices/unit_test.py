import unittest
from .models import ServicesPrice
from lib.tests import AvendaTestCase
import datetime


class ServicePriceTest(AvendaTestCase):

    def setUp(self):
        now = datetime.datetime.now()
        self.data = dict(date_from=now, date_to=now, pattern_id=1, service_id=1, prices=100)

    def test_db(self):
        self.db(ServicesPrice, self.data)

if __name__ == '__main__':
    unittest.main()
