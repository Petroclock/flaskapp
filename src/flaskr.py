# coding=utf-8;
import os
import sys

sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from flask import request, redirect, url_for, render_template, session
from flask import Flask
from flask_script import Manager
import app_config

from flask import session
from users.models import new_user, get_user
from database import db_session
from accounts.blueprint import accounts_app
from carts.blueprint import carts_app
from companies.blueprint import companies_app
from users.blueprint import users_app
from login_logs.blueprint import login_logs_app
from logs.blueprint import logs_app
from messages.blueprint import messages_app
from orders.blueprint import orders_app
from patterns.blueprint import patterns_app
from payment_methods.blueprint import payment_methods_app
from receipts.blueprint import receipts_app
from reservations_types.blueprint import reservation_types_app
from slots_types.blueprint import slots_types_app
from transactions.blueprint import transactions_app
from one_time_passwords.blueprint import one_time_passwords_app
from promo_code.blueprint import promo_codes_app
from services.blueprint import services_app
from services_prices.blueprint import services_prices_app
from users_groups.blueprint import users_groups_app
from sessions.blueprint import sessions_app
from users_companies_settings.blueprint import users_companies_settings_app
from plants.blueprint import plants_app
from bookings.blueprint import bookings_app
from reservations.blueprint import reservations_app
from slots.blueprint import slots_app

app = Flask(__name__)
app.config.update(
    DEBUG=app_config.DEBUG,
    SECRET_KEY=app_config.SECRET_KEY,
    TESTING=app_config.TESTING,
)

app.register_blueprint(companies_app, url_prefix='/api/v2/companies')
app.register_blueprint(users_app, url_prefix='/api/v2/users')
app.register_blueprint(login_logs_app, url_prefix='/api/v2/login_logs')
app.register_blueprint(one_time_passwords_app, url_prefix='/api/v2/one_time_passwords')
app.register_blueprint(promo_codes_app, url_prefix='/api/v2/promo_codes')
app.register_blueprint(services_app, url_prefix='/api/v2/services')
app.register_blueprint(services_prices_app, url_prefix='/api/v2/services_prices')
app.register_blueprint(users_groups_app, url_prefix='/api/v2/users_groups')
app.register_blueprint(sessions_app, url_prefix='/api/v2/sessions')
app.register_blueprint(users_companies_settings_app, url_prefix='/api/v2/users-companies-settings')
app.register_blueprint(plants_app, url_prefix='/api/v2/plants')
app.register_blueprint(bookings_app, url_prefix='/api/v2/bookings')
app.register_blueprint(reservations_app, url_prefix='/api/v2/reservations')
app.register_blueprint(slots_app, url_prefix='/api/v2/slots')
app.register_blueprint(accounts_app, url_prefix='/api/v2/accounts')
app.register_blueprint(carts_app, url_prefix='/api/v2/carts')
app.register_blueprint(logs_app, url_prefix='/api/v2/logs')
app.register_blueprint(messages_app, url_prefix='/api/v2/messages')
app.register_blueprint(orders_app, url_prefix='/api/v2/orders')
app.register_blueprint(patterns_app, url_prefix='/api/v2/patterns')
app.register_blueprint(payment_methods_app, url_prefix='/api/v2/payment_methods')
app.register_blueprint(receipts_app, url_prefix='/api/v2/receipts')
app.register_blueprint(reservation_types_app, url_prefix='/api/v2/reservation_types')
app.register_blueprint(slots_types_app, url_prefix='/api/v2/slots_types')
app.register_blueprint(transactions_app, url_prefix='/api/v2/transactions')


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.route('/initdb')
def create_database():
    from database import init_db
    init_db()
    return 'Database created'


@app.route('/')
def index():
    return 'Hello from Avenda API v.2'


@app.route('/registration', methods=['GET', 'POST'])
def registration():
    if request.method == 'POST':
        user = new_user(request.form)
        if user:
            return redirect(url_for("login"))
    # TODO вернуть нужный шаблон
    return redirect(url_for("index"))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        log_in = request.form["login"]
        password = request.form["password"]
        user = get_user(log_in, password)
        if user:
            session['user_id'] = user.id
            session['is_login'] = True
            return redirect(url_for("index"))
    session['user_id'] = None
    session['is_login'] = False
    # TODO вернуть нужный шаблон
    return redirect(url_for("index"))


@app.route('/logout', methods=['GET'])
def logout():
    session['user_id'] = None
    session['is_login'] = False
    return redirect(url_for("index"))


manager = Manager(app)


@manager.command
def run_test():
    import unittest
    from unittest import TestSuite
    from companies.unit_test import CompanyTest
    from login_logs.unit_test import LogsTest
    from one_time_passwords.unit_test import OneTimePassTest
    from promo_code.unit_test import PromoCodeTest
    from services.unit_test import ServiceTest
    from services_prices.unit_test import ServicePriceTest
    from sessions.unit_test import SessionTest
    from users_companies_settings.unit_test import UsersSettingTest
    from users_groups.unit_test import UserGroupTest
    from plants.unit_test import PlantTest
    from bookings.unit_test import BookingTest
    from reservations.unit_test import ReservationTest
    from slots.unit_test import SlotTest
    from lib.unit_test import DecoratorTest, TestRequest
    from accounts.unit_test import AccountTest
    from carts.unit_test import CartTest
    from logs.unit_test import LogTest
    from messages.unit_test import MessageTest
    from orders.unit_test import OrderTest
    from patterns.unit_test import PatternTest
    from payment_methods.unit_test import PaymentMethodTest
    from receipts.unit_test import ReceiptTest
    from reservations_types.unit_test import ReservationTypeTest
    from slots_types.unit_test import SlotsTypeTest
    from transactions.unit_test import TransactionTest

    def load_tests(loader, tests, pattern):
        suite = TestSuite()
        for test_class in tests:
            tests = loader.loadTestsFromTestCase(test_class)
            suite.addTests(tests)
        return suite

    # suite = unittest.TestLoader().loadTestsFromTestCase(CompanyTest)
    tests = (CompanyTest, LogsTest, OneTimePassTest, PromoCodeTest, ServiceTest,
             ServicePriceTest, SessionTest, UsersSettingTest, UserGroupTest,
             PlantTest, DecoratorTest, TestRequest, BookingTest, ReservationTest, SlotTest,
             AccountTest, CartTest, LogTest, MessageTest, OrderTest, PatternTest, PaymentMethodTest,
             ReceiptTest, ReservationTypeTest, SlotsTypeTest, TransactionTest)
    suite = load_tests(unittest.TestLoader(), tests, None)
    unittest.TextTestRunner(verbosity=2).run(suite)


if __name__ == "__main__":
    manager.run()
