from database import Base
from sqlalchemy import Column, Integer, ForeignKey, DateTime


class Receipt(Base):

    __tablename__ = 'receipts'

    id = Column(Integer, primary_key=True)
    booking_id = Column(Integer, ForeignKey('bookings.id'))
    booking_number = Column(Integer)
    number = Column(Integer)
    added_date = Column(DateTime)
    added_user = Column(Integer, ForeignKey('users.id'))

    def __init__(self, **kwargs):
        self.booking_id = kwargs.get('booking_id')
        self.booking_number = kwargs.get('booking_number')
        self.number = kwargs.get('number')
        self.added_date = kwargs.get('added_date')
        self.added_user = kwargs.get('added_user')
