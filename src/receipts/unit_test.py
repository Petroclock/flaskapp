from receipts.models import Receipt
from lib.tests import AvendaTestCase
from flaskr import db_session


class ReceiptTest(AvendaTestCase):
    def setUp(self):
        self.user = self.create_user()
        self.comp = self.get_or_create_company(self.user.id)
        self.service = self.create_service(self.comp.id)
        self.plant = self.create_plant(self.comp.id)
        self.book = self.create_book(self.comp.id, self.user.id, self.service.id, self.plant.id)
        self.data = {'added_user': self.user.id, 'booking_id': self.book.id}
        self.link = '/api/v2/receipts/'
        self.pass_through_outlet_link = self.link + 'pass-through-outlet/'
        self._items_to_delete = [self.book, self.plant, self.service, self.comp, self.user]
        self.model = Receipt

    def test_db(self):
        self.db(self.model, self.data)

    def test_pass_through_outlet(self):
        receipt = self.create_insert(self.model, self.data)
        data = {'data': {'id': receipt.id}}
        self.request_success(self.pass_through_outlet_link, data)
        db_session.add(self.book)
        self.assertEqual(self.book.receipts_used, 1)
        self.delete_row(receipt)
