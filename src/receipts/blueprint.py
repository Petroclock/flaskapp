from flask import Blueprint
from receipts.models import Receipt


receipts_app = Blueprint('Receipts', __name__)
from .views import pass_through_outlet
