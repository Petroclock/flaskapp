# coding=utf-8;

from flask import request
from sqlalchemy.orm.exc import NoResultFound

from database import db_session
from lib.request import RequestParams
from lib.helpers import json_success, json_fail
from lib.decorators import json_response

from .blueprint import receipts_app

from bookings.models import Booking
from .models import Receipt


@receipts_app.route('/pass-through-outlet/', methods=['POST'], endpoint='pass_through_outlet')
@json_response
def pass_through_outlet():
    try:
        data = RequestParams(request)
        talon_id = data.data['id']

        receipt = Receipt.query.filter(Receipt.id == talon_id).one()
        booking = Booking.query.filter(Booking.id == receipt.booking_id).one()
        booking.receipts_used += 1
        db_session.commit()
        return json_success([])
    except TypeError:
        return json_fail('Empty request')
    except NoResultFound:
        return json_fail('Not found')
    except KeyError:
        return json_fail('Empty request: no keys')
    except AssertionError:
        return json_fail('No valid value')

